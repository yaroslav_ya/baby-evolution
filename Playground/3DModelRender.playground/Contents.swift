import UIKit
import AVFoundation
import XCPlayground
import PlaygroundSupport

var page = 10

var currentPageChanged: ((_ page: Int) -> ())?

currentPageChanged = { x in
    _ = x
}

currentPageChanged?(page)

let containerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 375.0, height: 667.0))

let playerView = UIView(frame: CGRect(x: 10, y: 10 , width: containerView.bounds.width / 3, height: containerView.bounds.height / 3))

let intencity = 10.0

PlaygroundPage.current.liveView = containerView

let videoURL = Bundle.main.url(forResource: "1080p", withExtension: "mov")

let avAsset = AVAsset(url: videoURL!)
let playerItem = AVPlayerItem(asset: avAsset)
let player = AVQueuePlayer(items: [playerItem])
let playeLayer = AVPlayerLayer(player: player)
playerView.backgroundColor = UIColor.blue
playeLayer.frame = playerView.bounds

let shadowPath = UIBezierPath(rect: playerView.bounds)
playerView.layer.masksToBounds = false
playerView.layer.shadowColor = UIColor.black.cgColor
playerView.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
playerView.layer.shadowOpacity = 1
playerView.layer.shadowRadius = 6.0 //* CGFloat(intencity)
playerView.layer.shadowPath = shadowPath.cgPath

containerView.backgroundColor = UIColor.white
containerView.addSubview(playerView)
playerView.layer.addSublayer(playeLayer)
let _ = AVPlayerLooper(player: player, templateItem: playerItem)
//        player.seek(to: CMTime.zero)
player.play()







