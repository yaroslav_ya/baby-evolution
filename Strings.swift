// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum ARScene: StoryboardType {
    internal static let storyboardName = "ARScene"

    internal static let arSceneViewController = SceneType<ARSceneViewController>(storyboard: ARScene.self, identifier: "ARSceneViewController")
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum Payment: StoryboardType {
    internal static let storyboardName = "Payment"

    internal static let paymentViewController = SceneType<PaymentViewController>(storyboard: Payment.self, identifier: "PaymentViewController")

    internal static let promoPackViewController = SceneType<PromoPackViewController>(storyboard: Payment.self, identifier: "PromoPackViewController")
  }
  internal enum Settings: StoryboardType {
    internal static let storyboardName = "Settings"

    internal static let childBirthDateViewController = SceneType<ChildBirthDateViewController>(storyboard: Settings.self, identifier: "ChildBirthDateViewController")

    internal static let childNameViewController = SceneType<ChildNameViewController>(storyboard: Settings.self, identifier: "ChildNameViewController")

    internal static let legalInformationViewController = SceneType<LegalInformationViewController>(storyboard: Settings.self, identifier: "LegalInformationViewController")

    internal static let settingsTableViewController = SceneType<SettingsTableViewController>(storyboard: Settings.self, identifier: "SettingsTableViewController")

    internal static let textContentSelectionTableViewController = SceneType<TextContentSelectionTableViewController>(storyboard: Settings.self, identifier: "TextContentSelectionTableViewController")

    internal static let textContentViewController = SceneType<TextContentViewController>(storyboard: Settings.self, identifier: "TextContentViewController")

    internal static let videoTutorialViewController = SceneType<VideoTutorialViewController>(storyboard: Settings.self, identifier: "VideoTutorialViewController")
  }
  internal enum WelcomeSettings: StoryboardType {
    internal static let storyboardName = "WelcomeSettings"

    internal static let childSetupBirthDateViewController = SceneType<ChildSetupBirthDateViewController>(storyboard: WelcomeSettings.self, identifier: "ChildSetupBirthDateViewController")

    internal static let childSetupDoneViewController = SceneType<ChildSetupDoneViewController>(storyboard: WelcomeSettings.self, identifier: "ChildSetupDoneViewController")

    internal static let childSetupExplanationViewController = SceneType<ChildSetupExplanationViewController>(storyboard: WelcomeSettings.self, identifier: "ChildSetupExplanationViewController")

    internal static let childSetupNameViewController = SceneType<ChildSetupNameViewController>(storyboard: WelcomeSettings.self, identifier: "ChildSetupNameViewController")

    internal static let secondtimeCommerWelcomeViewController = SceneType<SecondtimeCommerWelcomeViewController>(storyboard: WelcomeSettings.self, identifier: "SecondtimeCommerWelcomeViewController")

    internal static let welcomeViewController = SceneType<WelcomeViewController>(storyboard: WelcomeSettings.self, identifier: "WelcomeViewController")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: Bundle(for: BundleToken.self))
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

private final class BundleToken {}
