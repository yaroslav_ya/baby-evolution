// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum ARScene: StoryboardType {
    internal static let storyboardName = "ARScene"

    internal static let arSceneViewController = SceneType<BabyEvolution.ARSceneViewController>(storyboard: ARScene.self, identifier: "ARSceneViewController")
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum Payment: StoryboardType {
    internal static let storyboardName = "Payment"

    internal static let paymentViewController = SceneType<BabyEvolution.PaymentViewController>(storyboard: Payment.self, identifier: "PaymentViewController")

    internal static let promoPackViewController = SceneType<BabyEvolution.PromoPackViewController>(storyboard: Payment.self, identifier: "PromoPackViewController")
  }
  internal enum Settings: StoryboardType {
    internal static let storyboardName = "Settings"

    internal static let legalInformationViewController = SceneType<BabyEvolution.LegalInformationViewController>(storyboard: Settings.self, identifier: "LegalInformationViewController")

    internal static let settingsTableViewController = SceneType<BabyEvolution.SettingsTableViewController>(storyboard: Settings.self, identifier: "SettingsTableViewController")
  }
  internal enum WelcomeSettings: StoryboardType {
    internal static let storyboardName = "WelcomeSettings"

    internal static let secondtimeCommerWelcomeViewController = SceneType<BabyEvolution.SecondtimeCommerWelcomeViewController>(storyboard: WelcomeSettings.self, identifier: "SecondtimeCommerWelcomeViewController")

    internal static let tutorialCompareViewController = SceneType<BabyEvolution.TutorialCompareViewController>(storyboard: WelcomeSettings.self, identifier: "TutorialCompareViewController")

    internal static let tutorialDoneViewController = SceneType<BabyEvolution.TutorialDoneViewController>(storyboard: WelcomeSettings.self, identifier: "TutorialDoneViewController")

    internal static let tutorialPlaceModelViewController = SceneType<BabyEvolution.TutorialPlaceModelViewController>(storyboard: WelcomeSettings.self, identifier: "TutorialPlaceModelViewController")

    internal static let tutorialRotateViewController = SceneType<BabyEvolution.TutorialRotateViewController>(storyboard: WelcomeSettings.self, identifier: "TutorialRotateViewController")

    internal static let welcomeViewController = SceneType<BabyEvolution.WelcomeViewController>(storyboard: WelcomeSettings.self, identifier: "WelcomeViewController")
  }
  internal enum WelcomeVideos: StoryboardType {
    internal static let storyboardName = "WelcomeVideos"

    internal static let tutorialCompareViewController = SceneType<BabyEvolution.TutorialCompareViewController>(storyboard: WelcomeVideos.self, identifier: "TutorialCompareViewController")

    internal static let tutorialPlaceModelViewController = SceneType<BabyEvolution.TutorialPlaceModelViewController>(storyboard: WelcomeVideos.self, identifier: "TutorialPlaceModelViewController")

    internal static let tutorialRotateViewController = SceneType<BabyEvolution.TutorialRotateViewController>(storyboard: WelcomeVideos.self, identifier: "TutorialRotateViewController")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: Bundle(for: BundleToken.self))
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

private final class BundleToken {}
