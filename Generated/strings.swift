// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {
  /// 1 Week
  internal static let week01 = L10n.tr("Localizable", "week01")
  /// 2 Week
  internal static let week02 = L10n.tr("Localizable", "week02")
  /// 5 Week
  internal static let week05 = L10n.tr("Localizable", "week05")
  /// 6 Week
  internal static let week06 = L10n.tr("Localizable", "week06")
  /// 7 Week
  internal static let week07 = L10n.tr("Localizable", "week07")
  /// 8 Week
  internal static let week08 = L10n.tr("Localizable", "week08")
  /// 9 Week
  internal static let week09 = L10n.tr("Localizable", "week09")
  /// 10 Week
  internal static let week10 = L10n.tr("Localizable", "week10")
  /// 11 Week
  internal static let week11 = L10n.tr("Localizable", "week11")
  /// 14 Week
  internal static let week14 = L10n.tr("Localizable", "week14")
  /// 16 Week
  internal static let week16 = L10n.tr("Localizable", "week16")
  /// 21 Week
  internal static let week21 = L10n.tr("Localizable", "week21")
  /// 24 Week
  internal static let week24 = L10n.tr("Localizable", "week24")
  /// 29 Week
  internal static let week29 = L10n.tr("Localizable", "week29")
  /// 40 Week
  internal static let week40 = L10n.tr("Localizable", "week40")
  /// 42 Week
  internal static let week42 = L10n.tr("Localizable", "week42")

  internal enum ARSceneViewController {
    internal enum PlaceVirtualObject {
      internal enum StatusViewController {
        /// CANNOT PLACE OBJECT\nTry moving left or right."
        internal static let showMessage = L10n.tr("Localizable", "ARSceneViewController.placeVirtualObject.statusViewController.showMessage")
      }
    }
    internal enum ResetTracking {
      internal enum ScheduleMessage {
        /// FIND A SURFACE TO PLACE AN OBJECT
        internal static let findASurface = L10n.tr("Localizable", "ARSceneViewController.resetTracking.scheduleMessage.findASurface")
        /// TRY MOVING LEFT OR RIGHT
        internal static let tryToMove = L10n.tr("Localizable", "ARSceneViewController.resetTracking.scheduleMessage.tryToMove")
      }
    }
  }

  internal enum ARSceneViewControllerTimerLogic {
    internal enum ManageRateUsAlert {
      internal enum RateUsAllertController {
        /// Are you engoing our app?
        internal static let message = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsAllertController.message")
        /// Are you ok?
        internal static let title = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsAllertController.title")
      }
      internal enum RateUsBadAction {
        /// No
        internal static let title = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.title")
        internal enum RateUsBadFlowAlertCancelAction {
          /// Cancel
          internal static let title = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsBadFlowAlertCancelAction.title")
        }
        internal enum RateUsBadFlowAlertController {
          /// Sorry to hear that you're not enjoing our app. Would you like to tell us why?
          internal static let message = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsBadFlowAlertController.message")
          /// We're Sorry
          internal static let title = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsBadFlowAlertController.title")
        }
        internal enum RateUsBadFlowAlertEmailAction {
          /// Email
          internal static let title = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsBadFlowAlertEmailAction.title")
          internal enum EmailHandler {
            internal enum EmailSubject {
              /// Support Request
              internal static let bad = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsBadFlowAlertEmailAction.emailHandler.emailSubject.bad")
              /// Support Request
              internal static let neutral = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsBadFlowAlertEmailAction.emailHandler.emailSubject.neutral")
            }
          }
        }
        internal enum RateUsGoodFlowAlertController {
          /// We're happy to hear that! Would you rate us in the App Store?
          internal static let message = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsGoodFlowAlertController.message")
          /// Hooray
          internal static let title = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsGoodFlowAlertController.title")
        }
        internal enum RateUsGoodFlowCancelAction {
          /// Cancel
          internal static let title = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsGoodFlowCancelAction.title")
        }
        internal enum RateUsGoodFlowRateAction {
          /// Rate Us
          internal static let title = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsGoodFlowRateAction.title")
        }
      }
      internal enum RateUsGoodAction {
        /// Yes
        internal static let title = L10n.tr("Localizable", "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsGoodAction.title")
      }
    }
  }

  internal enum ArSceneARSCNViewDelegate {
    internal enum Renderer {
      /// TAP + TO PLACE AN OBJECT
      internal static let scheduleMessage = L10n.tr("Localizable", "ArScene+ARSCNViewDelegate.renderer.scheduleMessage")
      /// SURFACE DETECTED
      internal static let showMessage = L10n.tr("Localizable", "ArScene+ARSCNViewDelegate.renderer.showMessage")
    }
    internal enum Session {
      /// The AR session failed.
      internal static let displayErrorMessage = L10n.tr("Localizable", "ArScene+ARSCNViewDelegate.session.displayErrorMessage")
    }
  }

  internal enum IAPHelper {
    internal enum Paymentable {
      internal enum FailedPurchaseHandler {
        internal enum AlertCancelAction {
          /// Cancel
          internal static let title = L10n.tr("Localizable", "IAPHelper.Paymentable.failedPurchaseHandler.alertCancelAction.title")
        }
        internal enum AlertController {
          /// We've failed to process your purchase
          internal static let message = L10n.tr("Localizable", "IAPHelper.Paymentable.failedPurchaseHandler.alertController.message")
          /// Fail to purchase
          internal static let title = L10n.tr("Localizable", "IAPHelper.Paymentable.failedPurchaseHandler.alertController.title")
        }
        internal enum AlertRetryAction {
          /// Retry
          internal static let title = L10n.tr("Localizable", "IAPHelper.Paymentable.failedPurchaseHandler.alertRetryAction.title")
        }
      }
    }
  }

  internal enum PaymentStoryboard {
    internal enum PaymentViewController {
      internal enum NavigationItem {
        /// PaymentStoryboard.PaymentViewController.navigationItem.title
        internal static let title = L10n.tr("Localizable", "PaymentStoryboard.PaymentViewController.navigationItem.title")
      }
    }
  }

  internal enum PaymentViewController {
    internal enum HandleTapOnPackView {
      internal enum NotIsProductPurchased {
        internal enum PurchaseErrorAlert {
          /// Sorry.\nProduct are unavailable to purchase.
          internal static let message = L10n.tr("Localizable", "PaymentViewController.handleTapOnPackView.notIsProductPurchased.purchaseErrorAlert.message")
          /// Purchase Error
          internal static let title = L10n.tr("Localizable", "PaymentViewController.handleTapOnPackView.notIsProductPurchased.purchaseErrorAlert.title")
        }
      }
    }
    internal enum NextButtonTapped {
      internal enum NotDidPurchased {
        internal enum AlertAction {
          /// Continue
          internal static let title = L10n.tr("Localizable", "PaymentViewController.nextButtonTapped.notDidPurchased.alertAction.title")
        }
        internal enum AlertViewController {
          /// PaymentViewController.nextButtonTapped.notDidPurchased.alertViewController.message
          internal static let message = L10n.tr("Localizable", "PaymentViewController.nextButtonTapped.notDidPurchased.alertViewController.message")
          /// PaymentViewController.nextButtonTapped.notDidPurchased.alertViewController.title
          internal static let title = L10n.tr("Localizable", "PaymentViewController.nextButtonTapped.notDidPurchased.alertViewController.title")
        }
      }
    }
    internal enum Produt {
      internal enum DidPurchased {
        /// Purchased
        internal static let `true` = L10n.tr("Localizable", "PaymentViewController.produt.didPurchased.true")
      }
    }
    internal enum ProdutView {
      internal enum FirstPackTitle {
        internal enum DidPurchased {
          /// Initial Pregnancy Period
          internal static let `true` = L10n.tr("Localizable", "PaymentViewController.produtView.firstPackTitle.didPurchased.true")
        }
      }
      internal enum GetProductButton {
        internal enum DidPurchased {
          /// Get
          internal static let `false` = L10n.tr("Localizable", "PaymentViewController.produtView.getProductButton.didPurchased.false")
          /// Choose
          internal static let `true` = L10n.tr("Localizable", "PaymentViewController.produtView.getProductButton.didPurchased.true")
        }
      }
      internal enum SecondPackTitle {
        internal enum DidPurchased {
          /// Interim Pregnancy Period
          internal static let `true` = L10n.tr("Localizable", "PaymentViewController.produtView.secondPackTitle.didPurchased.true")
        }
      }
      internal enum ThirdPackTitle {
        internal enum DidPurchased {
          /// Final Pregnancy Period
          internal static let `true` = L10n.tr("Localizable", "PaymentViewController.produtView.thirdPackTitle.didPurchased.true")
        }
      }
    }
    internal enum RestorePurchases {
      internal enum AlertController {
        /// You've successfuly restore your purchases!
        internal static let message = L10n.tr("Localizable", "PaymentViewController.restorePurchases.alertController.message")
      }
      internal enum NotSuccess {
        internal enum AlertAction {
          /// Continue
          internal static let title = L10n.tr("Localizable", "PaymentViewController.restorePurchases.notSuccess.alertAction.title")
        }
        internal enum AlertController {
          /// Purchases restoration have been failed.\nSorry.
          internal static let message = L10n.tr("Localizable", "PaymentViewController.restorePurchases.notSuccess.alertController.message")
          /// Purchase Restore Failedtle
          internal static let title = L10n.tr("Localizable", "PaymentViewController.restorePurchases.notSuccess.alertController.title")
        }
      }
      internal enum Success {
        internal enum AlertAction {
          /// Continue
          internal static let title = L10n.tr("Localizable", "PaymentViewController.restorePurchases.success.alertAction.title")
        }
        internal enum AlertController {
          /// Purchase Restore Succeed
          internal static let title = L10n.tr("Localizable", "PaymentViewController.restorePurchases.success.alertController.title")
        }
      }
    }
    internal enum ViewDidAppear {
      internal enum CancelAlertAction {
        /// Cancel
        internal static let title = L10n.tr("Localizable", "PaymentViewController.viewDidAppear.cancelAlertAction.title")
      }
      internal enum RestorePurchaseAlertAction {
        /// Restore
        internal static let title = L10n.tr("Localizable", "PaymentViewController.viewDidAppear.restorePurchaseAlertAction.title")
      }
      internal enum RestorePurchaseUserPushingAlert {
        /// You've previously have purchased this application, please restore your purchases othervise you'll be pushed to limited version of the App
        internal static let message = L10n.tr("Localizable", "PaymentViewController.viewDidAppear.restorePurchaseUserPushingAlert.message")
        /// Restore Purchases
        internal static let title = L10n.tr("Localizable", "PaymentViewController.viewDidAppear.restorePurchaseUserPushingAlert.title")
      }
    }
  }

  internal enum PromoPackViewController {
    internal enum BuyButtonView {
      /// PromoPackViewController.BuyButtonView.getProductButtonTitle
      internal static let getProductButtonTitle = L10n.tr("Localizable", "PromoPackViewController.BuyButtonView.getProductButtonTitle")
    }
  }

  internal enum SettingsStoryboard {
    internal enum LegalInformationViewController {
      internal enum NavigationItem {
        /// Legal Information
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.LegalInformationViewController.navigationItem.title")
      }
    }
    internal enum SettingsTableViewController {
      internal enum AboutTableViewCell {
        /// About
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.SettingsTableViewController.AboutTableViewCell.title")
      }
      internal enum AgreementTableViewCell {
        /// Agreement
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.SettingsTableViewController.AgreementTableViewCell.title")
      }
      internal enum FactsTableViewCell {
        /// Write Us
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.SettingsTableViewController.FactsTableViewCell.title")
      }
      internal enum LicensesTableViewCell {
        /// Licenses
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.SettingsTableViewController.LicensesTableViewCell.title")
      }
      internal enum PurchasesTableViewCell {
        /// SettingsStoryboard.SettingsTableViewController.PurchasesTableViewCell.title
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.SettingsTableViewController.PurchasesTableViewCell.title")
      }
      internal enum VideoTutorialTableViewCell {
        /// SettingsStoryboard.SettingsTableViewController.VideoTutorialTableViewCell.title
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.SettingsTableViewController.VideoTutorialTableViewCell.title")
      }
      internal enum NavigationItem {
        /// SettingsStoryboard.SettingsTableViewController.navigationItem.title
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.SettingsTableViewController.navigationItem.title")
      }
    }
    internal enum TextContentSelectionTableViewController {
      internal enum NavigationItem {
        /// Facts
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.TextContentSelectionTableViewController.navigationItem.title")
      }
    }
    internal enum TextContentViewController {
      internal enum NavigationItem {
        /// SettingsStoryboard.TextContentViewController.navigationItem.title
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.TextContentViewController.navigationItem.title")
      }
    }
    internal enum VideoTutorialViewController {
      internal enum NavigationItem {
        /// Tutorial
        internal static let title = L10n.tr("Localizable", "SettingsStoryboard.VideoTutorialViewController.navigationItem.title")
      }
    }
  }

  internal enum StatusViewController {
    internal enum ARCamera {
      internal enum TrackingState {
        internal enum PresentationString {
          /// TRACKING NORMAL
          internal static let normal = L10n.tr("Localizable", "StatusViewController.ARCamera.TrackingState.presentationString.normal")
          /// TRACKING UNAVAILABLE
          internal static let notAvailable = L10n.tr("Localizable", "StatusViewController.ARCamera.TrackingState.presentationString.notAvailable")
          internal enum Limited {
            /// TRACKING LIMITED\nExcessive motion
            internal static let excessiveMotion = L10n.tr("Localizable", "StatusViewController.ARCamera.TrackingState.presentationString.limited(.excessiveMotion)")
            /// Initializing
            internal static let initializing = L10n.tr("Localizable", "StatusViewController.ARCamera.TrackingState.presentationString.limited(.initializing)")
            /// TRACKING LIMITED\nLow detail
            internal static let insufficientFeatures = L10n.tr("Localizable", "StatusViewController.ARCamera.TrackingState.presentationString.limited(.insufficientFeatures)")
            /// Recovering from interruption
            internal static let relocalizing = L10n.tr("Localizable", "StatusViewController.ARCamera.TrackingState.presentationString.limited(.relocalizing)")
          }
        }
        internal enum Recommendation {
          internal enum Limited {
            /// Try slowing down your movement, or reset the session.
            internal static let excessiveMotion = L10n.tr("Localizable", "StatusViewController.ARCamera.TrackingState.recommendation.limited(.excessiveMotion)")
            /// Try pointing at a flat surface, or reset the session.
            internal static let insufficientFeatures = L10n.tr("Localizable", "StatusViewController.ARCamera.TrackingState.recommendation.limited(.insufficientFeatures)")
            /// Return to the location where you left off or try resetting the session.
            internal static let relocalizing = L10n.tr("Localizable", "StatusViewController.ARCamera.TrackingState.recommendation.limited(.relocalizing)")
          }
        }
      }
    }
  }

  internal enum WelcomeStoryboard {
    internal enum ChildSetupBirthDateViewController {
      internal enum Header {
        /// WelcomeStoryboard.ChildSetupBirthDateViewController.header.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupBirthDateViewController.header.text")
      }
      internal enum NextButton {
        internal enum TitleLabel {
          /// WelcomeStoryboard.ChildSetupBirthDateViewController.nextButton.titleLabel.text
          internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupBirthDateViewController.nextButton.titleLabel.text")
        }
      }
      internal enum TextBody {
        /// WelcomeStoryboard.ChildSetupBirthDateViewController.textBody.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupBirthDateViewController.textBody.text")
      }
    }
    internal enum ChildSetupDoneViewController {
      internal enum Header {
        /// WelcomeStoryboard.ChildSetupDoneViewController.header.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupDoneViewController.header.text")
      }
      internal enum NextButton {
        internal enum TitleLabel {
          /// WelcomeStoryboard.ChildSetupDoneViewController.nextButton.titleLabel.text
          internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupDoneViewController.nextButton.titleLabel.text")
        }
      }
      internal enum TextBody {
        /// WelcomeStoryboard.ChildSetupDoneViewController.textBody.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupDoneViewController.textBody.text")
      }
    }
    internal enum ChildSetupExplanationViewController {
      internal enum Header {
        /// WelcomeStoryboard.ChildSetupExplanationViewController.header.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupExplanationViewController.header.text")
      }
      internal enum NextButton {
        internal enum TitleLabel {
          /// WelcomeStoryboard.ChildSetupExplanationViewController.nextButton.titleLabel.text
          internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupExplanationViewController.nextButton.titleLabel.text")
        }
      }
      internal enum TextBody {
        /// WelcomeStoryboard.ChildSetupExplanationViewController.textBody.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupExplanationViewController.textBody.text")
      }
    }
    internal enum ChildSetupNameViewController {
      internal enum Header {
        /// WelcomeStoryboard.ChildSetupNameViewController.header.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupNameViewController.header.text")
      }
      internal enum NextButton {
        internal enum TitleLabel {
          /// WelcomeStoryboard.ChildSetupNameViewController.nextButton.titleLabel.text
          internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupNameViewController.nextButton.titleLabel.text")
        }
      }
      internal enum TextBody {
        /// WelcomeStoryboard.ChildSetupNameViewController.textBody.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.ChildSetupNameViewController.textBody.text")
      }
    }
    internal enum SecondCommerWelcomeViewController {
      internal enum Header {
        /// WelcomeStoryboard.SecondCommerWelcomeViewController.header.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.SecondCommerWelcomeViewController.header.text")
      }
      internal enum NextButton {
        internal enum TitleLabel {
          /// WelcomeStoryboard.SecondCommerWelcomeViewController.nextButton.titleLabel.text
          internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.SecondCommerWelcomeViewController.nextButton.titleLabel.text")
        }
      }
      internal enum TextBody {
        /// WelcomeStoryboard.SecondCommerWelcomeViewController.textBody.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.SecondCommerWelcomeViewController.textBody.text")
      }
    }
    internal enum WelcomeViewController {
      internal enum Header {
        /// WelcomeStoryboard.WelcomeViewController.header.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.WelcomeViewController.header.text")
      }
      internal enum NextButton {
        internal enum TitleLabel {
          /// WelcomeStoryboard.WelcomeViewController.nextButton.titleLabel.text
          internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.WelcomeViewController.nextButton.titleLabel.text")
        }
      }
      internal enum TextBody {
        /// WelcomeStoryboard.WelcomeViewController.textBody.text
        internal static let text = L10n.tr("Localizable", "WelcomeStoryboard.WelcomeViewController.textBody.text")
      }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
