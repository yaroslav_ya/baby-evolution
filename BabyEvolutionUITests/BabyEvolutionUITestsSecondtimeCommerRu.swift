//
//  BabyEvolutionUITestsSecondTimeCommerRu.swift
//  BabyEvolutionUITests
//
//  Created by Yaroslav on 03/09/2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import XCTest


class BabyEvolutionUITestsSecondtimeCommerRu: XCTestCase {

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        let app = XCUIApplication()
        
        app.launchArguments = [
            "-inUITest",
            "-AppleLanguages",
            "(ru)",
            "-AppleLocale",
            "ru",
            "SECOND_WORKFLOW_TEST"
        ]
        
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = true

        
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testThatSecondtimeCommerFlowLocalizaitonWorksWell() {

        let app = XCUIApplication()
        let cellTitles = [
            "Имя малыша",
            "Дата родов",
            "Покупки",
            "Интересные факты",
            "Видео инструкция",
            "О программе",
            "Соглашение",
            "Лицензии"
        ]
        
        /// Welcome screen Localization tests
        XCTAssertTrue(app.staticTexts.element(boundBy: 0).label == "С возвращением",
                      "Fail Header text with: \(app.staticTexts.element(boundBy: 0).label)")
        XCTAssertTrue((app.textViews.element(boundBy: 0).value as! String).contains("Эмбрион поправился на 10 килограмм."),
                      "Fail body text with \(app.staticTexts.element(boundBy: 0).label)")
        XCTAssertTrue(app.buttons.element(boundBy: 0).label == "Поехали" ,
                      "Fail button text")
        app.buttons["Поехали"].tap()
        
        /// ARScene screen Localization tests
        app.buttons["Подробнее"].tap()

        /// Settings main screen Localization tests
        let tablesQuery = app.tables
        
        for cell in tablesQuery.statusItems.allElementsBoundByIndex {
            XCTAssertTrue(cellTitles.contains(cell.title), "Cell name failed \(cell.title)")
        }
        let backToSettingsButton = app.navigationBars.element(boundBy: 0).buttons.element(boundBy: 0)
        tablesQuery.staticTexts["Имя малыша"].tap()
        
        //        navigationBarTitle = app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label
        XCTAssertTrue(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label == "Имя",
                      "Fail navigation bar label text with \(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label)")
        /// Settings child name screen Localization tests
        backToSettingsButton.tap()
        tablesQuery.staticTexts["Дата родов"].tap()
        
        /// Settings birth date screen Localization tests
        XCTAssertTrue(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label == "Дата родов",
                      "Fail navigation bar label text with \(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label)")
        backToSettingsButton.tap()
        tablesQuery.staticTexts["Покупки"].tap()
        
        /// Settings purchases screen Localization tests
        XCTAssertTrue(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label == "Покупки",
                      "Fail navigation bar title text with \(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label)")
        backToSettingsButton.tap()
        tablesQuery.staticTexts["Интересные факты"].tap()
        
        /// Settings interesting facts screen Localization tests
        XCTAssertTrue(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label == "Факты",
                      "Fail navigation bar label text with \(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label)")
        backToSettingsButton.tap()
        tablesQuery.staticTexts["Видео инструкция"].tap()
        
        /// Settings video tutorial screen Localization tests
        XCTAssertTrue(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label == "Инструкция",
                      "Fail navigation bar title text with \(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label)")
        backToSettingsButton.tap()
        tablesQuery.staticTexts["О программе"].tap()
        
        /// Settings about screen Localization tests
        XCTAssertTrue(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label == "О программе",
                      "Fail navigation bar label text with \(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label)")
        backToSettingsButton.tap()
        tablesQuery.cells.staticTexts["Соглашение"].tap()
        
        /// Settings agreement screen Localization tests
        XCTAssertTrue(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label == "Соглашение",
                      "Fail navigation bar label text with \(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label)")
        backToSettingsButton.tap()
        tablesQuery.staticTexts["Лицензии"].tap()
        
        /// Settings licenses screen Localization tests
        XCTAssertTrue(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label == "Лицензии",
                      "Fail navigation bar label text with \(app.navigationBars.element(boundBy: 0).staticTexts.element(boundBy: 0).label)")
        backToSettingsButton.tap()

    }

}
