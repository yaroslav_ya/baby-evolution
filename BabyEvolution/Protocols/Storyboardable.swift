//
//  Storyboardable.swift
//  BabyEvolution
//
//  Created by Yaroslav on 28.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import os.log

protocol Storyboardable {
    var coordinator: Coordinatorable? { get set }
    static func instantiate(from storyboard: UIStoryboard) -> Self
}

extension Storyboardable where Self: UIViewController {
    static func instantiate(from storyboard: UIStoryboard) -> Self {
        let fullModuleName = NSStringFromClass(self)
        let className = fullModuleName.components(separatedBy: ".").last!
        return storyboard.instantiateViewController(withIdentifier: className) as! Self // swiftlint:disable:this force_cast
    }
}
