//
//  File.swift
//  BabyEvolution
//
//  Created by Yaroslav on 28.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import StoreKit
import os.log

protocol Coordinatorable {
    var window: UIWindow? { get set }
    var childCoordinators: [Coordinatorable] { get set }
    var parentCoordinator: Coordinatorable? { get set }
    
    var navigationController: UINavigationController { get set }
    
    func start()
    func pushView(causedBy sender: Any, tappedIn viewController: UIViewController)
}


