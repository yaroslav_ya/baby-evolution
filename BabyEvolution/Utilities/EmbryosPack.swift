//
//  EmbryosPack.swift
//  BabyEvolution
//
//  Created by Yaroslav on 17/09/2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import Foundation
import StoreKit
import SceneKit

enum AppStoreProductIdentifier: String, CaseIterable {
    case firstEmbrioPack = "ru.neatness.embryoevolution.first_ar_content_pack"
    case secondEmbrioPack = "ru.neatness.embryoevolution.second_ar_content_pack"
    case thirdEmbrioPack = "ru.neatness.embryoevolution.third_ar_content_pack"
    case allEmbrioPacks = "ru.neatness.embryoevolution.complete_ar_content_pack"
    
    init(from modelsPack: PackNumber) {
        switch modelsPack {
        case .first: self = .firstEmbrioPack
        case .second: self = .secondEmbrioPack
        case .third: self = .thirdEmbrioPack
        case .all: self = .allEmbrioPacks
        }
    }
    
    init(from packID: PackIdentifier) {
        switch packID {
        case .firstPack: self = .firstEmbrioPack
        case .secondPack: self = .secondEmbrioPack
        case .thirdPack: self = .thirdEmbrioPack
        case .allPacks: self = .allEmbrioPacks
        }
    }
    
    func returnNumber() -> PackNumber {
        switch self {
        case .firstEmbrioPack: return .first
        case .secondEmbrioPack: return .second
        case .thirdEmbrioPack: return .third
        case .allEmbrioPacks: return .all
        }
    }
}

enum PackNumber: Int, Codable, CaseIterable {
    case first, second, third, all
}

enum PackIdentifier: String, CaseIterable {
    case firstPack
    case secondPack
    case thirdPack
    case allPacks
    init(from identifier: String) {
        guard
            let packIDsub = identifier.split(separator: ".").first
            else { fatalError("Wrong View restorationID format") }
        switch String(packIDsub) {
        case PackIdentifier.firstPack.rawValue: self = .firstPack
        case PackIdentifier.secondPack.rawValue: self = .secondPack
        case PackIdentifier.thirdPack.rawValue: self = .thirdPack
        case PackIdentifier.allPacks.rawValue: self = .allPacks
        default: fatalError("Wrong view RestorationID")
        }
    }
    
    init(from appStoreIdentifier: AppStoreProductIdentifier) {
        switch appStoreIdentifier {
        case .firstEmbrioPack: self = .firstPack
        case .secondEmbrioPack: self = .secondPack
        case .thirdEmbrioPack: self = .thirdPack
        case .allEmbrioPacks: self = .allPacks
        }
    }
}


public struct EmbryosPack {
    static func isPurchased(_ product: AppStoreProductIdentifier) -> Bool {
        return store.purchasedProductIdentifiers.contains(product.rawValue)
    }

    
    private static let productIdentifiers: Set<ProductIdentifier> =
        [AppStoreProductIdentifier.firstEmbrioPack.rawValue,
         AppStoreProductIdentifier.secondEmbrioPack.rawValue,
         AppStoreProductIdentifier.thirdEmbrioPack.rawValue,
         AppStoreProductIdentifier.allEmbrioPacks.rawValue]
    
    public static let store = IAPHelper(productIds: EmbryosPack.productIdentifiers)
}


extension SKProduct {
    var isPurchased: Bool {
        get {
            return EmbryosPack.store.isProductPurchased(self.productIdentifier)
        }
    }
}


enum Model: String, Codable, CaseIterable {
    case week01, week02, week05, week06, week07 // 1 pack: 5 models
    case week08, week09, week10, week11, week14 // 2 pack: 5 models
    case week16, week21, week24, week29, week40 // 3 pack: 5 models
    case week42 // 3 pack bonus: 1 model
    
    static func modelsFromSelectedPack(_ pack: PackNumber) -> [Model] {
        switch pack {
        case .first:
            return [.week01, .week02, .week05, .week06, .week07]
        case .second:
            return [.week08, .week09, .week10, .week11, .week14]
        case .third:
            if EmbryosPack.isPurchased(.firstEmbrioPack)
                && EmbryosPack.isPurchased(.secondEmbrioPack)
                && EmbryosPack.isPurchased(.thirdEmbrioPack)
                || EmbryosPack.isPurchased(.allEmbrioPacks) {
                return [.week16, .week21, .week24, .week29, .week40, week42]
            } else {
                return [.week16, .week21, .week24, .week29, .week40, week42]
            }
        case .all:
            return [.week01, .week02, .week05, .week06, .week07,
                    .week08, .week09, .week10, .week11, .week14,
                    .week16, .week21, .week24, .week29, .week40, week42]
        }
    }
    
    static func returnScale(for model: Model) -> SCNVector3 {
        switch model {
        case .week01: return SCNVector3(0.01, 0.01, 0.01)
        case .week02: return SCNVector3(0.025, 0.025, 0.025)
        case .week05: return SCNVector3(0.05, 0.05, 0.05)
        case .week06: return SCNVector3(0.075, 0.075, 0.075)
        case .week07: return SCNVector3(0.1, 0.1, 0.1)
            
        case .week08: return SCNVector3(0.1, 0.1, 0.1)
        case .week09: return SCNVector3(0.125, 0.125, 0.125)
        case .week10: return SCNVector3(0.15, 0.15, 0.15)
        case .week11: return SCNVector3(0.175, 0.175, 0.175)
        case .week14: return SCNVector3(0.2, 0.2, 0.2)
            
        case .week16: return SCNVector3(0.20, 0.20, 0.20)
        case .week21: return SCNVector3(0.25, 0.25, 0.25)
        case .week24: return SCNVector3(0.3, 0.3, 0.3)
        case .week29: return SCNVector3(0.4, 0.4, 0.4)
        case .week40: return SCNVector3(0.5, 0.5, 0.5)
        case .week42: return SCNVector3(0.5, 0.5, 0.5)
        }
    }
    
    static func modelAvailableInTrial(in pack: PackNumber) -> [Model] {
        switch pack {
        case .first:
            return [.week01,]
        case .second:
            return [.week14,]
        case .third:
            return [.week40,]
        case .all:
            return [.week42,]
        }
    }
}
