//
//  PGViewController.swift
//  BabyEvolution
//
//  Created by Yaroslav on 30.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import AVFoundation

class PGViewController: UIViewController, Storyboardable {
    var coordinator: Coordinatorable?
    var playerLooper: AVPlayerLooper?
    
    func playTurorial(from url: URL, in videoView: UIView) {
        let avAsset = AVAsset(url: url)
        let playerItem = AVPlayerItem(asset: avAsset)
        let player = AVQueuePlayer(items: [playerItem])
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = videoView.bounds
        videoView.layer.masksToBounds = true
        videoView.layer.cornerRadius = 16
        videoView.layer.insertSublayer(playerLayer, below: videoView.layer)
        playerLooper = AVPlayerLooper(player: player, templateItem: playerItem)
        player.play()
    }
    
    func prepareView(_ view: UIView) {
        view.layer.cornerRadius = 16
        view.backgroundColor = UIColor.clear
    }
    
}


extension UIView {
    @objc
    func setGradient(with startGradientColor: UIColor, endGradientColor: UIColor) {
        self.clipsToBounds = true
        var subView: CAGradientLayer?
        if let sublayers = self.layer.sublayers,
            sublayers.contains(where: { (layer) -> Bool in
                return type(of: layer) == CAGradientLayer.self
            }) {
            subView = self.layer.sublayers!.filter { (layer) -> Bool in
                return type(of: layer) == CAGradientLayer.self
                }.first as? CAGradientLayer
        } else {
            subView = CAGradientLayer()
            self.layer.insertSublayer(subView!, at: 0)
        }
        subView!.frame = self.bounds
        
        subView!.colors = [startGradientColor.cgColor, endGradientColor.cgColor]
    }
    
    
    /// This method drop shadow with given color
    /// This method implicitly set Clip to Bounds to false
    ///
    /// - Parameters:
    ///   - color: Shadow color **UIColor**
    @objc
    func dropShadow(with color: UIColor = #colorLiteral(red: 0.5411764706, green: 0.3529411765, blue: 0.4078431373, alpha: 0.7)) {
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.layer.shadowOpacity = 0.7
        self.layer.shadowRadius = 6.0
        self.layer.shadowPath = shadowPath.cgPath
    }
    
    
    /// This method drop shadow with given radius
    ///
    /// - Parameter radius: radius of the shadow
    @objc
    func videoDropShadow(with radius: CGFloat = 8) {
        self.layer.cornerRadius = 16
        let shadowPath = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = radius
        self.layer.shadowPath = shadowPath.cgPath
    }

}
