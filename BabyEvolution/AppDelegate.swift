//
//  AppDelegate.swift
//  BabyEvolution
//
//  Created by Yaroslav on 19.07.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import os.log
import SwiftKeychainWrapper
import YandexMobileMetrica

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var threeModelTrialDuration: TimeInterval?
    var fullTestModelTrialAccessDuration: TimeInterval?
    
    let customKeychainInstance = KeychainWrapper(serviceName: "ru.neatness.baby-evolution")
    
    var window: UIWindow?
    var coordinator: Coordinatorable?
    var navigationController: UINavigationController?
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let args = ProcessInfo().arguments
        navigationController = UINavigationController()

        guard let configuration = YMMYandexMetricaConfiguration(apiKey: APP_METRIKA_KEY) else { fatalError("Unable to init AppMetrica with the current API key") }
        configuration.statisticsSending = true
        configuration.locationTracking = false
        YMMYandexMetrica.activate(with: configuration)
        
        if let firstAppLaunchDate = customKeychainInstance.double(forKey: KeychainKeys.appFirstLaunchDate.rawValue),
            firstAppLaunchDate < Date().timeIntervalSinceReferenceDate {

            os_log("First app launch date %f",
                   firstAppLaunchDate)
        } else {
            let _ = customKeychainInstance.set(Date().timeIntervalSinceReferenceDate,
                                               forKey: KeychainKeys.appFirstLaunchDate.rawValue)
            let _ = customKeychainInstance.set(ApplicaitonState.justStarted.rawValue,
                                               forKey: KeychainKeys.applicationState.rawValue)
            UserDefaults.standard.set(Flow.newcommerFlow.rawValue, forKey: UserDefaultsKeys.userFlowState.rawValue)
        }

        coordinator = LaunchCoordinator(navigationController: navigationController!, parentCoordinator: nil)
        os_log("App lauched with args: ", args)
        if args.contains("SECOND_WORKFLOW_TEST") {
            UserDefaults.standard.set(Flow.secondtimeCommerFlow.rawValue, forKey: UserDefaultsKeys.userFlowState.rawValue)
        }

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        coordinator?.start()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        if let viewController = navigationController?.topViewController as? ARSceneViewController {
            viewController.blurView.isHidden = false
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        if let viewController = navigationController?.topViewController as? ARSceneViewController {
            viewController.blurView.isHidden = false
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        if let viewController = navigationController?.topViewController as? ARSceneViewController {
            viewController.manageApplicationState()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        if let viewController = navigationController?.topViewController as? ARSceneViewController {
            viewController.blurView.isHidden = true
            viewController.manageApplicationState()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

}
