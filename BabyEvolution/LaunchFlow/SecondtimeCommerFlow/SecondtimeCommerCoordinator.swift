//
//  OldfagCoordinator.swift
//  BabyEvolution
//
//  Created by Yaroslav on 28.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import StoreKit
import os.log

protocol LaunchCoordinatorable {
    var secondCommerVC: UIViewController? { get set }
    var promoPackVC: PromoPackViewController? { get set }
    var paymentVC: PaymentViewController? { get set }
    var newcommerVC: UIViewController? { get set }
}

class SecondtimeCommerCoordinator: Coordinatorable {
    var parentCoordinator: Coordinatorable?
    
    var window: UIWindow?
    
    private let welcomeStoryboard = StoryboardScene.WelcomeSettings.storyboard
    private let paymentStoryboard = StoryboardScene.Payment.storyboard
    
    var childCoordinators = [Coordinatorable]()
    
    var delegate: LaunchCoordinatorable?
    
    var products: [String: SKProduct]?
    
    var secondtimeCommerVC: SecondtimeCommerWelcomeViewController
    var promoPackVC: PromoPackViewController
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController, parentCoordinator: Coordinatorable?) {
        self.navigationController = navigationController
        self.parentCoordinator = parentCoordinator
        secondtimeCommerVC = SecondtimeCommerWelcomeViewController.instantiate(from: welcomeStoryboard)
        promoPackVC = PromoPackViewController.instantiate(from: paymentStoryboard)
        delegate?.promoPackVC = promoPackVC
    }
    
    func start() {
        secondtimeCommerVC.coordinator = self
        navigationController.pushViewController(secondtimeCommerVC, animated: true)
    }
    
    func pushView(causedBy sender: Any, tappedIn viewController: UIViewController) {
        switch NSStringFromClass(type(of: viewController)) {
        case NSStringFromClass(type(of: secondtimeCommerVC) as AnyClass):
            
            if EmbryosPack.isPurchased(.firstEmbrioPack)
                && EmbryosPack.isPurchased(.secondEmbrioPack)
                && EmbryosPack.isPurchased(.thirdEmbrioPack)
                || EmbryosPack.isPurchased(.allEmbrioPacks) {
                changeWorkflow()
            } else {
                changeWorkflow()
            }
        default:
           changeWorkflow()
        }
    }
    
    func changeWorkflow() {
        guard let parentCoordinator = parentCoordinator as? LaunchCoordinator else { return }
        delegate?.secondCommerVC = secondtimeCommerVC
        delegate?.promoPackVC = promoPackVC
        parentCoordinator.flow = .universalFlow
        parentCoordinator.start()
    }
}
