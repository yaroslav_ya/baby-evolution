//
//  LaunchCoordinator.swift
//  BabyEvolution
//
//  Created by Yaroslav on 28.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import StoreKit
import SwiftKeychainWrapper
import os.log

class LaunchCoordinator: Coordinatorable, LaunchCoordinatorable {
    var secondCommerVC: UIViewController?
    
    var newcommerVC: UIViewController?
    
    var paymentVC: PaymentViewController?
    
    var window: UIWindow?
    
    let customKeychainInstance = KeychainWrapper(serviceName: "ru.neatness.baby-evolution")
    
    private let paymentStoryboard = StoryboardScene.Payment.storyboard
    
    var promoPackVC: PromoPackViewController?
    
    let dispatchGroup = DispatchGroup()
    
    var parentCoordinator: Coordinatorable?
    
    var childCoordinators = [Coordinatorable]()
    
    var optionalNewcommerCoordinator: NewcommerFlowCoordinator?
    
    var optionalSecondtimeCommerCoordinator: SecondtimeCommerCoordinator?
    
    var optionalUnversalCoordinator: UniversalFlowCoordinator?

    let arScene = ARSceneViewController.instantiate(from: StoryboardScene.ARScene.storyboard)
    
    
    var flow = Flow(rawValue: (UserDefaults.standard.integer(forKey: UserDefaultsKeys.userFlowState.rawValue)))!
    
    var products: [String: SKProduct]?
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController, parentCoordinator: Coordinatorable?) {
        self.navigationController = navigationController
    }
    
    func pushView(causedBy sender: Any, tappedIn viewController: UIViewController) {
        fatalError("This \(#function) isn't ment to be ran ever.")
    }

    func start() {
        if !navigationController.viewControllers.contains(arScene) {
            navigationController.pushViewController(arScene, animated: true)
        }
        chooseUserFlow()
    }
    
    @objc func promoNotificationHandler(_ notification: Notification) {
        
    }
    
    func chooseUserFlow() {
        switch flow {
        case .newcommerFlow: runNewcommerFlow()
        case .secondtimeCommerFlow: runSecondtimeCommerFlow()
        case .universalFlow: runUniversalFlow()
        case .educationRetryFlow: runEducationRetryFlow()
        }
    }
    
    func runNewcommerFlow() {

        if let _ = optionalNewcommerCoordinator {
        } else {
        optionalNewcommerCoordinator = NewcommerFlowCoordinator(navigationController: navigationController,
                                 parentCoordinator: self)
        }
        if let _ = paymentVC {
        } else {
            paymentVC = PaymentViewController.instantiate(from: paymentStoryboard)
        }
        if customKeychainInstance.integer(forKey: KeychainKeys.applicationState.rawValue)! == ApplicaitonState.justStarted.rawValue {
            optionalNewcommerCoordinator?.isAppStart = true
        } else {        
            optionalNewcommerCoordinator?.isAppStart = false
        }
        optionalNewcommerCoordinator!.window = window
        optionalNewcommerCoordinator!.paymentVC = paymentVC
        optionalNewcommerCoordinator!.delegate = self
        optionalNewcommerCoordinator!.start()
    }
    
    func runSecondtimeCommerFlow() {
        
        if let _ = optionalSecondtimeCommerCoordinator {
            
        } else {
            optionalSecondtimeCommerCoordinator = SecondtimeCommerCoordinator(navigationController: navigationController,
                                                               parentCoordinator: self)
        }
        
        optionalSecondtimeCommerCoordinator!.window = window
        optionalSecondtimeCommerCoordinator!.delegate = self
        optionalSecondtimeCommerCoordinator!.start()
    }
    
    func runUniversalFlow() {
        if let _ = optionalUnversalCoordinator {
        } else {
            optionalUnversalCoordinator = UniversalFlowCoordinator(navigationController: navigationController,
                                                            parentCoordinator: self)
        }
        
        optionalUnversalCoordinator!.secondCommerVC = secondCommerVC
        optionalUnversalCoordinator!.arSceneVC = arScene
        optionalUnversalCoordinator!.newcommerVC = newcommerVC
        optionalUnversalCoordinator!.promoPackVC = promoPackVC
        optionalUnversalCoordinator!.paymentVC = paymentVC
        optionalUnversalCoordinator!.start()
    }
    
    func runEducationRetryFlow() {
        childCoordinators[3].start()
    }
    
}


