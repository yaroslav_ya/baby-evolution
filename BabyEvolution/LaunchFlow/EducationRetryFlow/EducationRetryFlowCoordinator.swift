//
//  EducationRetryFlowCoordinator.swift
//  BabyEvolution
//
//  Created by Yaroslav on 30.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import os.log

class EducationRetryFlowCoordinator: Coordinatorable {
    var window: UIWindow?
    
    var childCoordinators = [Coordinatorable]()
    
    var parentCoordinator: Coordinatorable?
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController, parentCoordinator: Coordinatorable?) {
        self.parentCoordinator = parentCoordinator
        self.navigationController = navigationController
    }
    
    func start() {
        
    }
    
    func pushView(causedBy sender: Any, tappedIn viewController: UIViewController) {
        
    }
    
    
}
