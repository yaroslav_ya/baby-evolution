//
//  NewCommerCoordinator.swift
//  BabyEvolution
//
//  Created by Yaroslav on 28.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import StoreKit
import os.log

class NewcommerFlowCoordinator: Coordinatorable {
    var parentCoordinator: Coordinatorable?
    
    var window: UIWindow?
    
    var isAppStart = false
    
    var delegate: LaunchCoordinatorable?
    
    var childCoordinators = [Coordinatorable]()
    var products: [String: SKProduct]?
    let welcomeVC: WelcomeViewController
    let childExplainVC: TutorialPlaceModelViewController
    let childBirthVC: TutorialRotateViewController
    let childNameVC: TutorialCompareViewController
    let childSetupDoneVC: TutorialDoneViewController
    
    var legalInfoVC: LegalInformationViewController?
    
    var paymentVC: PaymentViewController?
    
    var navigationController: UINavigationController
    
    
    private let storyboard = UIStoryboard.init(name: "WelcomeSettings", bundle: .main )
    private let paymentStoryboard = StoryboardScene.Payment.storyboard
    private let settingsStotyboard = StoryboardScene.Settings.storyboard
    private let videoStoryboard = StoryboardScene.WelcomeVideos.storyboard

    init(navigationController: UINavigationController, parentCoordinator: Coordinatorable?) {
        self.navigationController = navigationController
        self.parentCoordinator = parentCoordinator
        self.welcomeVC = WelcomeViewController.instantiate(from: storyboard)
        self.childExplainVC = TutorialPlaceModelViewController.instantiate(from: storyboard)
        self.childBirthVC = TutorialRotateViewController.instantiate(from: storyboard)
        self.childNameVC = TutorialCompareViewController.instantiate(from: storyboard)
        self.childSetupDoneVC = TutorialDoneViewController.instantiate(from: storyboard)
        delegate?.paymentVC = paymentVC
        NotificationCenter.default.addObserver(self, selector: #selector(productSucceedRequestHandler(_:)), name: .PGIAPHelperProductsInfoDeliveredNotification, object: nil)
    }
    
    func start() {
        welcomeVC.coordinator = self
        welcomeVC.navigationController?.title = NSLocalizedString("WelcomeStoryboard.WelcomeViewController.header.text", comment: "")
        navigationController.pushViewController(welcomeVC, animated: true)
    }
    
    @objc func promoNotificationHandler(_ notification: Notification) {
        
    }
    
    @objc func productSucceedRequestHandler(_ notification: Notification) {
        
    }
    
    func pushView(causedBy sender: Any, tappedIn viewController: UIViewController) {
        switch NSStringFromClass(type(of: viewController)) {
        case NSStringFromClass(type(of: welcomeVC)):
            if let _ = sender as? UITapGestureRecognizer {
                if let _ = legalInfoVC {
                } else {
                    legalInfoVC = LegalInformationViewController.instantiate(from: settingsStotyboard)
                }
                legalInfoVC?.coordinator = self
                legalInfoVC?.navigationItemTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.AgreementTableViewCell.title", comment: "")
                legalInfoVC?.legalText = LEGAL_TEXT_AGREEMENT
                navigationController.pushViewController(legalInfoVC!, animated: true)
            } else {
                legalInfoVC?.navigationItemTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.AgreementTableViewCell.title", comment: "")
                childExplainVC.coordinator = self
                navigationController.pushViewController(childExplainVC, animated: true)
            }
        case NSStringFromClass(type(of: childExplainVC)):
            childBirthVC.coordinator = self
            navigationController.pushViewController(childBirthVC, animated: true)
        case NSStringFromClass(type(of: childBirthVC)):
            childNameVC.coordinator = self
            navigationController.pushViewController(childNameVC, animated: true)
        case NSStringFromClass(type(of: childNameVC)):
            childSetupDoneVC.coordinator = self
            navigationController.pushViewController(childSetupDoneVC, animated: true)
        case NSStringFromClass(type(of: childSetupDoneVC)):
            if isAppStart {
                fallthrough
            } else {
                fallthrough
            }
        default:
            guard let parentCoordinator = parentCoordinator as? LaunchCoordinator else { return }
            delegate?.paymentVC = paymentVC
            delegate?.newcommerVC = childSetupDoneVC
            parentCoordinator.flow = .universalFlow
            parentCoordinator.start()
        }
    }
    
}
