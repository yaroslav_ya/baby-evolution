//
//  NeutralFlowCoordinator.swift
//  BabyEvolution
//
//  Created by Yaroslav on 29.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import StoreKit
import os.log


class UniversalFlowCoordinator: Coordinatorable {
    var childCoordinators = [Coordinatorable]()
    
    var window: UIWindow?
    
    var delegate: LaunchCoordinatorable?
    
    private let sceneStoryboard = StoryboardScene.ARScene.storyboard
    private let rSceneStoryboard = StoryboardScene.ARScene.storyboardName
    private let settingsStoryboard = StoryboardScene.Settings.storyboard
    private let rSettingsStoryboard = StoryboardScene.Settings.storyboardName
    private let welcomeStoryboard = StoryboardScene.WelcomeSettings.storyboard
    private let paymentStoryboard = StoryboardScene.Payment.storyboard
        
    var parentCoordinator: Coordinatorable?
    
    private enum BuyButtonsIdentifier: String {
        case nextPaymentButton = "paymentViewController.NextButton"
        case secondPackBuyButton = "secondPack.BuyButton"
        case thirdPackBuyButton = "thirdPack.BuyButton"
        case allPacksBuyButton = "allPacks.BuyButton"
    }
    
    
    
    var timers: [Timer]?
    
    var products: [String: SKProduct]?
    
    var navigationController: UINavigationController
    
    var arSceneVC: ARSceneViewController?
    var secondCommerVC: UIViewController?
    var paymentVC: PaymentViewController?
    var promoPackVC: PromoPackViewController?
    var newcommerVC: UIViewController?
    var settingsVC: SettingsTableViewController?
    var acknowledgeVC: LegalInformationViewController?
    
    init(navigationController: UINavigationController, parentCoordinator: Coordinatorable?) {
        self.parentCoordinator = parentCoordinator
        self.navigationController = navigationController
        NotificationCenter.default.addObserver(self, selector: #selector(promoNotificationHandler(_:)), name: .PGPromoTimerFireNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(productSucceedRequestHandler(_:)), name: .PGIAPHelperProductsInfoDeliveredNotification, object: nil)
    }
    
    func start() {
        prepare(arSceneVC)
        if UserDefaults.standard.integer(forKey: UserDefaultsKeys.userFlowState.rawValue) == Flow.newcommerFlow.rawValue {        
            UserDefaults.standard.set(Flow.secondtimeCommerFlow.rawValue,
                                      forKey: UserDefaultsKeys.userFlowState.rawValue)
        }
        navigationController.popToRootViewController(animated: true)
    }
    
    func prepare<T>(_ viewController: T) {
        if let viewController = viewController as? PGViewController {
            viewController.coordinator = self
        } else if let viewController = viewController as? PGTableViewController {
            viewController.coordinator = self
        }
    }
    
    @objc func promoNotificationHandler(_ notification: Notification) {
        pushView(causedBy: notification, tappedIn: arSceneVC!)
    }
    
    @objc func productSucceedRequestHandler(_ notification: Notification) {
        
    }
    
    func pushView(causedBy sender: Any , tappedIn viewController: UIViewController) {
        
        guard
            let senderObject = sender as? UIView,
            let senderObjcetIdentifier = senderObject.restorationIdentifier,
            let strippedIdentifier = senderObjcetIdentifier.split(separator: ".").last else {
                guard let _ = arSceneVC!.view.window else { return }
                if let _ = promoPackVC {
                } else {
                    promoPackVC = PromoPackViewController.instantiate(from: paymentStoryboard)
                }
                
                promoPackVC?.isOnStart = false
                promoPackVC?.delegate = arSceneVC!
                prepare(promoPackVC)
                promoPackVC?.modalPresentationStyle = .overCurrentContext
                arSceneVC!.present(promoPackVC!, animated: true)
                return
                
        }
        
        
        switch (strippedIdentifier, viewController.restorationIdentifier) {
        case ("NextButton", "ARSceneViewController"):
            if let _ = paymentVC {
            } else {
                paymentVC = PaymentViewController.instantiate(from: paymentStoryboard)
            }
            paymentVC?.isOnStart = false
            prepare(paymentVC)
            navigationController.pushViewController(paymentVC!, animated: true)
        case ("NextButton", "PaymentViewController"), ("CancelButton","PaymentViewController"):
            arSceneVC!.restartExperience()
            prepare(arSceneVC)
            navigationController.popToViewController(arSceneVC!, animated: true)
        case ("View", "PaymentViewController"):
            arSceneVC!.restartExperience()
            prepare(arSceneVC)
            navigationController.popToViewController(arSceneVC!, animated: true)
        case ("SettingsButton", "PaymentViewController"):
            if let _ = settingsVC {
            } else {
                settingsVC = SettingsTableViewController.instantiate(from: settingsStoryboard)
            }
            prepare(settingsVC)
            navigationController.pushViewController(settingsVC!, animated: true)
        case ("VideoTutorial", "SettingsTableViewController"):
            navigationController.popToRootViewController(animated: true)
            changeWorkflow()
        case ("About", "SettingsTableViewController"):
            if let _ = acknowledgeVC {
                
            } else {
                acknowledgeVC = LegalInformationViewController.instantiate(from: settingsStoryboard)
            }
            prepare(acknowledgeVC!)
            acknowledgeVC!.navigationItemTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.AboutTableViewCell.title", comment: "About Screen header")
            acknowledgeVC!.legalText = LEGAL_TEXT_ABOUT
            navigationController.pushViewController(acknowledgeVC!, animated: true)
        case ("Agreement", "SettingsTableViewController"):
            if let _ = acknowledgeVC {

            } else {
                acknowledgeVC = LegalInformationViewController.instantiate(from: settingsStoryboard)
            }
            acknowledgeVC!.navigationItemTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.AgreementTableViewCell.title", comment: "Agreement Screen header")
            acknowledgeVC!.legalText = LEGAL_TEXT_AGREEMENT
            prepare(acknowledgeVC!)
            navigationController.pushViewController(acknowledgeVC!, animated: true)
        case ("License","SettingsTableViewController"):
            if let _ = acknowledgeVC {
                
            } else {
                acknowledgeVC = LegalInformationViewController.instantiate(from: settingsStoryboard)
            }
            acknowledgeVC!.navigationItemTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.LicensesTableViewCell.title", comment: "License Screen header")
            acknowledgeVC!.legalText = LEGAL_TEXT_LICENSE
            prepare(acknowledgeVC!)
            navigationController.pushViewController(acknowledgeVC!, animated: true)
        case ("DimensionType", "SettingsTableViewController"):
            return
        default:
            fatalError()
        }
        
    }
    
    func changeWorkflow() {
        guard let parentCoordinator = parentCoordinator as? LaunchCoordinator else { return }
        delegate?.secondCommerVC = secondCommerVC
        delegate?.promoPackVC = promoPackVC
        parentCoordinator.flow = .newcommerFlow
        parentCoordinator.start()
    }
}
