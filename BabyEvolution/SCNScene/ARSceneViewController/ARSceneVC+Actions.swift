//
//  ARSceneVC+Actions.swift
//  BabyEvolution
//
//  Created by Yaroslav on 05/09/2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import SceneKit
import os.log

extension ARSceneViewController: UIGestureRecognizerDelegate {
    
    enum SegueIdentifier: String {
        case showObjects
    }
    
    // MARK: - Interface Actions
    
    /// Displays the `VirtualObjectSelectionViewController` from the `addObjectButton` or in response to a tap gesture in the `sceneView`.
    @IBAction func showVirtualObjectSelectionViewController() {
        // Ensure adding objects is an available action and we are not loading another object (to avoid concurrent modifications of the scene).
        guard !addObjectButton.isHidden && !virtualObjectLoader.isLoading else { return }
        statusViewController.cancelScheduledMessage(for: .contentPlacement)
        performSegue(withIdentifier: SegueIdentifier.showObjects.rawValue, sender: addObjectButton)
    }
    
    /// Determines if the tap gesture for presenting the `VirtualObjectSelectionViewController` should be used.
    func gestureRecognizerShouldBegin(_: UIGestureRecognizer) -> Bool {
        return virtualObjectLoader.loadedObjects.isEmpty
    }
    
    func gestureRecognizer(_: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith _: UIGestureRecognizer) -> Bool {
        return true
    }
    
    /// - Tag: restartExperience
    func restartExperience() {
        guard isRestartAvailable, !virtualObjectLoader.isLoading else { return }
        isRestartAvailable = false
        
        statusViewController.cancelAllScheduledMessages()
        
        virtualObjectLoader.removeAllVirtualObjects()
        resetTracking()
        
        // Disable restart for a while in order to give the session time to restart.
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            self.isRestartAvailable = true
        }
    }
}

extension ARSceneViewController: UIPopoverPresentationControllerDelegate {
    
    // MARK: - UIPopoverPresentationControllerDelegate
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // All menus should be popovers (even on iPhone).
        
        guard let appState = customKeychainInstance.integer(forKey: KeychainKeys.applicationState.rawValue) else { fatalError("None AppState") }
        
        if let popoverController = segue.destination.popoverPresentationController,
            let button = sender as? UIButton {
            popoverController.delegate = self
            popoverController.sourceView = button
            popoverController.sourceRect = button.bounds
        }
        
        guard let identifier = segue.identifier,
            let segueIdentifer = SegueIdentifier(rawValue: identifier),
            segueIdentifer == .showObjects else { return }
        
        let modelPack = PackNumber(rawValue: UserDefaults.standard.integer(forKey: UserDefaultsKeys.selectedModelsPack.rawValue))!        
        
        let objectsTableViewController = segue.destination as! VirtualObjectSelectionTableViewController
        
        let objectsSet = VirtualObject.objectsIncluded(in: modelPack).sorted(by: { (objectOne, objectTwo) -> Bool in
            return objectOne.modelName < objectTwo.modelName
        })
        
        if appState == ApplicaitonState.trialLong.rawValue {
            objectsTableViewController.virtualObjects = VirtualObject.objectsAfterLongTrial(in: PackNumber.third)
        } else if appState == ApplicaitonState.trialNone.rawValue {
            // TODO: Add case implementation
            // for .trialNone
            objectsTableViewController.virtualObjects = VirtualObject.objectsAfterLongTrial(in: PackNumber.third)
        } else if appState == ApplicaitonState.trialBoth.rawValue
            || appState == ApplicaitonState.purchased.rawValue {
            objectsTableViewController.virtualObjects = objectsSet
        }
        objectsTableViewController.delegate = self
        self.objectsViewController = objectsTableViewController
        
        // Set all rows of currently placed objects to selected.
        for object in virtualObjectLoader.loadedObjects {
            guard let index = objectsSet.index(of: object) else { continue }
            objectsTableViewController.selectedVirtualObjectRows.insert(index)
        }
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        objectsViewController = nil
    }
}

