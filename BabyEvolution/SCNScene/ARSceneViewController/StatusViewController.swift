//
//  StatusViewController.swift
//  BabyEvolution
//
//  Created by Yaroslav on 04/09/2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import Foundation
import ARKit


class StatusViewController: UIViewController {
    
    // MARK: - Types
        
    enum MessageType {
        case trackingStateEscalation
        case planeEstimation
        case contentPlacement
        case focusSquare
        
        static var all: [MessageType] = [
            .trackingStateEscalation,
            .planeEstimation,
            .contentPlacement,
            .focusSquare
        ]
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var messagePanel: UIVisualEffectView!
    
    @IBOutlet weak private var messageLabel: UILabel!
    
    @IBOutlet weak private var restartExperienceButtons: UIButton!
    
    // MARK: - Properties
    
    /// Trigerred when the "Restart Experience" button is tapped.
    var restartExperienceHandler: () -> Void = {}
    
    /// Seconds before the timer message should fade out. Adjust if the app needs longer transient messages.
    private let displayDuration: TimeInterval = 6
    
    // Timer for hiding messages.
    private var messageHideTimer: Timer?
    
    private var timers: [MessageType: Timer] = [:]
    
    // MARK: - Message Handling
    
    func showMessage(_ text: String, autoHide: Bool = true) {
        // Cancel any previous hide timer.
        messageHideTimer?.invalidate()
        
        messageLabel.text = text
        
        // Make sure status is showing.
        setMessageHidden(false, animated: true)
        
        if autoHide {
            messageHideTimer = Timer.scheduledTimer(withTimeInterval: displayDuration, repeats: false, block: { [weak self] _ in
                self?.setMessageHidden(true, animated: true)
            })
        }
    }
    
    func scheduleMessage(_ text: String, inSeconds seconds: TimeInterval, messageType: MessageType) {
        cancelScheduledMessage(for: messageType)
        
        let timer = Timer.scheduledTimer(withTimeInterval: seconds, repeats: false, block: { [weak self] timer in
            self?.showMessage(text)
            timer.invalidate()
        })
        
        timers[messageType] = timer
    }
    
    func cancelScheduledMessage(for messageType: MessageType) {
        timers[messageType]?.invalidate()
        timers[messageType] = nil
    }
    
    func cancelAllScheduledMessages() {
        for messageType in MessageType.all {
            cancelScheduledMessage(for: messageType)
        }
    }
    
    // MARK: - ARKit
    
    func showTrackingQualityInfo(for trackingState: ARCamera.TrackingState, autoHide: Bool) {
        showMessage(trackingState.presentationString, autoHide: autoHide)
    }
    
    func escalateFeedback(for trackingState: ARCamera.TrackingState, inSeconds seconds: TimeInterval) {
        cancelScheduledMessage(for: .trackingStateEscalation)
        
        let timer = Timer.scheduledTimer(withTimeInterval: seconds, repeats: false, block: { [unowned self] _ in
            self.cancelScheduledMessage(for: .trackingStateEscalation)
            
            var message = trackingState.presentationString
            if let recommendation = trackingState.recommendation {
                message.append(": \(recommendation)")
            }
            
            self.showMessage(message, autoHide: false)
        })
        
        timers[.trackingStateEscalation] = timer
    }
    
    // MARK: - IBActions
    
    @IBAction private func restartExperience(_ sender: UIButton) {
        restartExperienceHandler()
    }
    
    // MARK: - Panel Visibility
    
    private func setMessageHidden(_ hide: Bool, animated: Bool) {
        // The panel starts out hidden, so show it before animating opacity.
        messagePanel.isHidden = false
        
        guard animated else {
            messagePanel.alpha = hide ? 0 : 1
            return
        }
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [.beginFromCurrentState], animations: {
            self.messagePanel.alpha = hide ? 0 : 1
        }, completion: nil)
    }
}

extension ARCamera.TrackingState {
    var presentationString: String {
        switch self {
        case .notAvailable:
            return NSLocalizedString("StatusViewController.ARCamera.TrackingState.presentationString.notAvailable", comment: "ARCamera.TrackingState.presentationString.notAvailable; default value: TRACKING UNAVAILABLE")
        case .normal:
            return NSLocalizedString("StatusViewController.ARCamera.TrackingState.presentationString.normal", comment: "ARCamera.TrackingState.presentationString.normal; default value: TRACKING NORMAL")
        case .limited(.excessiveMotion):
            return NSLocalizedString("StatusViewController.ARCamera.TrackingState.presentationString.limited(.excessiveMotion)", comment: "ARCamera.TrackingState.presentationString.limited(.excessiveMotion); default value: TRACKING LIMITED\nExcessive motion")
        case .limited(.insufficientFeatures):
            return NSLocalizedString("StatusViewController.ARCamera.TrackingState.presentationString.limited(.insufficientFeatures)", comment: "ARCamera.TrackingState.presentationString.limited(.insufficientFeatures); default value: TRACKING LIMITED\nLow detail")
        case .limited(.initializing):
            return NSLocalizedString("StatusViewController.ARCamera.TrackingState.presentationString.limited(.initializing)", comment: "ARCamera.TrackingState.presentationString.limited(.initializing); default value: Initializing")
        case .limited(.relocalizing):
            return NSLocalizedString("StatusViewController.ARCamera.TrackingState.presentationString.limited(.relocalizing)", comment: "ARCamera.TrackingState.presentationString.limited(.relocalizing); default value: Recovering from interruption")
        }
    }
    
    var recommendation: String? {
        switch self {
        case .limited(.excessiveMotion):
            return NSLocalizedString("StatusViewController.ARCamera.TrackingState.recommendation.limited(.excessiveMotion)", comment: "ARCamera.TrackingState.recommendation.limited(.excessiveMotion); default value: Try slowing down your movement, or reset the session.")
        case .limited(.insufficientFeatures):
            return NSLocalizedString("StatusViewController.ARCamera.TrackingState.recommendation.limited(.insufficientFeatures)", comment: "ARCamera.TrackingState.recommendation.limited(.insufficientFeatures); default value: Try pointing at a flat surface, or reset the session.")
        case .limited(.relocalizing):
            return NSLocalizedString("StatusViewController.ARCamera.TrackingState.recommendation.limited(.relocalizing)", comment: "ARCamera.TrackingState.recommendation.limited(.relocalizing); default value: Return to the location where you left off or try resetting the session.")
        default:
            return nil
        }
    }

    
}
