//
//  ARSceneViewController+ObjectSelection.swift
//  BabyEvolution
//
//  Created by Yaroslav on 05/09/2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import ARKit
import YandexMobileMetrica

extension ARSceneViewController: VirtualObjectSelectionTableViewControllerDelegate {
    /**
     Adds the specified virtual object to the scene, placed using
     the focus square's estimate of the world-space position
     currently corresponding to the center of the screen.
     
     - Tag: PlaceVirtualObject
     */
    func placeVirtualObject(_ virtualObject: VirtualObject) {
        guard let cameraTransform = session.currentFrame?.camera.transform,
            let focusSquareAlignment = focusSquare.recentFocusSquareAlignments.last,
            focusSquare.state != .initializing else {
                statusViewController.showMessage(NSLocalizedString("ARSceneViewController.placeVirtualObject.statusViewController.showMessage", comment: "ARSceneViewController.placeVirtualObject.statusViewController.showMessage, default: CANNOT PLACE OBJECT\nTry moving left or right."))
                if let controller = objectsViewController {
                    virtualObjectSelectionTableViewController(controller, didDeselectObject: virtualObject)
                }
                return
        }
        
        YMMYandexMetrica.reportEvent("\(type(of: self)) \(#function)",
            parameters: ["modelName": virtualObject.modelName],
            onFailure: nil)
        
        // The focus square transform may contain a scale component, so reset scale to 1
        let focusSquareScaleInverse = 1.0 / focusSquare.simdScale.x
        let scaleMatrix = float4x4(uniformScale: focusSquareScaleInverse)
        let focusSquareTransformWithoutScale = focusSquare.simdWorldTransform * scaleMatrix
        
        virtualObjectInteraction.selectedObject = virtualObject
        virtualObject.setTransform(focusSquareTransformWithoutScale,
                                   relativeTo: cameraTransform,
                                   smoothMovement: false,
                                   alignment: focusSquareAlignment,
                                   allowAnimation: false)
        
        updateQueue.async {
            // MARK: - Setting Mesh Scale
            if IS_REAL_DIMENSION {
                virtualObject.scale = Model.returnScale(for: Model(rawValue: virtualObject.modelName)!)
            } else {
                virtualObject.scale = SCNVector3(0.2, 0.2, 0.2)
            }
            self.sceneView.scene.rootNode.addChildNode(virtualObject)
            self.sceneView.addOrUpdateAnchor(for: virtualObject)
        }
    }
    
    // MARK: - VirtualObjectSelectionViewControllerDelegate
    
    func virtualObjectSelectionTableViewController(_: VirtualObjectSelectionTableViewController?, didSelectObject object: VirtualObject) {
        virtualObjectLoader.loadVirtualObject(object, loadedHandler: { [unowned self] loadedObject in
            self.sceneView.prepare([object], completionHandler: { _ in
                DispatchQueue.main.async {
                    self.hideObjectLoadingUI()
                    self.placeVirtualObject(loadedObject)
                }
            })
        })
        
        displayObjectLoadingUI()
    }
    
    func virtualObjectSelectionTableViewController(_: VirtualObjectSelectionTableViewController?, didDeselectObject object: VirtualObject) {
        guard let objectIndex = virtualObjectLoader.loadedObjects.index(of: object) else {
            fatalError("Programmer error: Failed to lookup virtual object in scene.")
        }
        virtualObjectLoader.removeVirtualObject(at: objectIndex)
        virtualObjectInteraction.selectedObject = nil
        if let anchor = object.anchor {
            session.remove(anchor: anchor)
        }
    }
    
    // MARK: Object Loading UI
    
    func displayObjectLoadingUI() {
        // Show progress indicator.
        spinner.startAnimating()
        
        addObjectButton.isHidden = true
        isRestartAvailable = false
    }
    
    func hideObjectLoadingUI() {
        // Hide progress indicator.
        spinner.stopAnimating()
        
        addObjectButton.isHidden = false
        isRestartAvailable = true
    }
}
