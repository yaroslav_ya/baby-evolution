//
//  ARSceneViewController+TimerLogic.swift
//  BabyEvolution
//
//  Created by Yaroslav on 25/09/2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import StoreKit
import os.log

// MARK: ARSceneViewController + Timers
extension ARSceneViewController: StateChangable {
    
    
    func changeAppStateOnPurchase() {
        manageApplicationState()
    }
    
    func manageApplicationState() {
        if let appStateRawValue = customKeychainInstance.integer(forKey: KeychainKeys.applicationState.rawValue),
            let appState = ApplicaitonState(rawValue: appStateRawValue) {
            guard let appLaunchDate = customKeychainInstance.double(forKey: KeychainKeys.appFirstLaunchDate.rawValue) else { fatalError("No app launch date") }
            
            switch appState {
                
            // MARK: State Before the first open ARSceneViewController
            case ApplicaitonState.justStarted:
                shortTrialPeriodTimer = Timer(timeInterval: SHOR_TRIAL_PERIOD_INTERVAL,
                                              target: self,
                                              selector: #selector(stopTrialBy(_:)),
                                              userInfo: ["ApplicationState": ApplicaitonState.purchased],
                                              repeats: false)
                longTrialPeriodTimer = Timer(timeInterval: LONG_TRIAL_INTERVAL,
                                             target: self,
                                             selector: #selector(stopTrialBy(_:)),
                                             userInfo: ["ApplicationState": ApplicaitonState.purchased],
                                             repeats: false)
                RunLoop.current.add(shortTrialPeriodTimer, forMode: .default)
                RunLoop.current.add(longTrialPeriodTimer, forMode: .default)
                if longTrialPeriodTimer.isValid && shortTrialPeriodTimer.isValid {
                    let _ = customKeychainInstance.set(ApplicaitonState.purchased.rawValue,
                                                       forKey: KeychainKeys.applicationState.rawValue)
                } else if longTrialPeriodTimer.isValid {
                    let _ = customKeychainInstance.set(ApplicaitonState.purchased.rawValue,
                                                       forKey: KeychainKeys.applicationState.rawValue)
                } else {
                    let _ = customKeychainInstance.set(ApplicaitonState.purchased.rawValue,
                                                       forKey: KeychainKeys.applicationState.rawValue)
                }
                
            // MARK: Models are all visible and transparent
            case ApplicaitonState.trialBoth:
                if longTrialPeriodTimer.isValid && shortTrialPeriodTimer.isValid {
                    shortTrialPeriodTimer.invalidate()
                    longTrialPeriodTimer.invalidate()
                }
                
                shortTrialPeriodTimer = Timer(timeInterval: (appLaunchDate + SHOR_TRIAL_PERIOD_INTERVAL) - Date().timeIntervalSinceReferenceDate,
                                              target: self,
                                              selector: #selector(stopTrialBy(_:)),
                                              userInfo: ["ApplicationState": ApplicaitonState.purchased],
                                              repeats: false)
                longTrialPeriodTimer = Timer(timeInterval: (appLaunchDate + LONG_TRIAL_INTERVAL) - Date().timeIntervalSinceReferenceDate,
                                             target: self,
                                             selector: #selector(stopTrialBy(_:)),
                                             userInfo: ["ApplicationState": ApplicaitonState.purchased],
                                             repeats: false)
                RunLoop.current.add(shortTrialPeriodTimer, forMode: .default)
                RunLoop.current.add(longTrialPeriodTimer, forMode: .default)
                
            // MARK: Model are only one visible and transparent
            case ApplicaitonState.trialLong:
                if longTrialPeriodTimer.isValid || shortTrialPeriodTimer.isValid {
                    longTrialPeriodTimer.invalidate()
                    shortTrialPeriodTimer.invalidate()
                }
                longTrialPeriodTimer = Timer(timeInterval: (appLaunchDate + LONG_TRIAL_INTERVAL) - Date().timeIntervalSinceReferenceDate,
                                             target: self,
                                             selector: #selector(stopTrialBy(_:)),
                                             userInfo: ["ApplicationState": ApplicaitonState.purchased],
                                             repeats: false)
                RunLoop.current.add(longTrialPeriodTimer, forMode: .default)
                
            // MARK: Model are only one visible and opaque
            case ApplicaitonState.trialNone:
                longTrialPeriodTimer = Timer(timeInterval: 0,
                                             target: self,
                                             selector: #selector(stopTrialBy(_:)),
                                             userInfo: ["ApplicationState": ApplicaitonState.purchased],
                                             repeats: false)
                stopTrialBy(longTrialPeriodTimer)
                
            // MARK: Out of trial workflow
            case ApplicaitonState.purchased:
                if shortTrialPeriodTimer.isValid || longTrialPeriodTimer.isValid {
                    shortTrialPeriodTimer.invalidate()
                    longTrialPeriodTimer.invalidate()
                }
            }
        }
    }
    
    
    func managePromoViewController() {
        if EmbryosPack.isPurchased(.firstEmbrioPack)
            && EmbryosPack.isPurchased(.secondEmbrioPack)
            && EmbryosPack.isPurchased(.thirdEmbrioPack)
            || EmbryosPack.isPurchased(.allEmbrioPacks) {
            promoPeriodTimer.invalidate()
        } else {
            promoPeriodTimer.invalidate()
        }
    }
    
    func manageRateUsAlert() {
//        rateUsPeriodTimer.invalidate()
//        rateUsPeriodTimer = Timer(timeInterval: rateUsPeriod,
//                                  target: self,
//                                  selector: #selector(runRateUsAlert(_:)),
//                                  userInfo: nil,
//                                  repeats: false)
//        RunLoop.current.add(rateUsPeriodTimer, forMode: .default)
    }
    
    
    @objc fileprivate func runRateUsAlert(_ timer: Timer) {
        guard !UserDefaults.standard.bool(forKey: UserDefaultsKeys.rateUsDidShow.rawValue) else { return }
        
        guard let appSateRaw = customKeychainInstance.integer(forKey: KeychainKeys.applicationState.rawValue) else {fatalError("None ApplicationState stored in Keychain")}
        let appState = ApplicaitonState(rawValue: appSateRaw)
        if appState == .purchased || appState == .trialBoth {
            let rateUsAlertController = UIAlertController(title: NSLocalizedString("ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsAllertController.title", comment: "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsAllertController.title, default: Are you ok?"), message: NSLocalizedString("ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsAllertController.message", comment: "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsAllertController.message, default: Are you engoing our app?"), preferredStyle: .alert)
            let rateUsBadAction = UIAlertAction(title: NSLocalizedString("ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.title", comment: "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.title, default: No"), style: .default) { (action) in
                let rateUsBadFlowEmailAction = UIAlertAction(title: NSLocalizedString("ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsBadFlowAlertEmailAction.title", comment: "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsBadFlowAlertEmailAction.title, default: Email"), style: .default, handler: { (action) in
                  self.sendEmail()
                })
                self.rateUsBadFlowAlertController.addAction(self.rateUsBadFlowCancelAction)
                self.rateUsBadFlowAlertController.addAction(rateUsBadFlowEmailAction)
                self.rateUsBadFlowAlertController.preferredAction = rateUsBadFlowEmailAction
                self.present(self.rateUsBadFlowAlertController, animated: true, completion: nil)
                UserDefaults.standard.set(true, forKey: UserDefaultsKeys.rateUsDidShow.rawValue)
            }
            let rateUsGoodAction = UIAlertAction(title: NSLocalizedString("ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsGoodAction.title", comment: "ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsGoodAction.title, default: Yes"), style: .default) { (action) in
                SKStoreReviewController.requestReview()
            }
            rateUsAlertController.addAction(rateUsBadAction)
            rateUsAlertController.addAction(rateUsGoodAction)
            rateUsAlertController.preferredAction = rateUsGoodAction
            present(rateUsAlertController, animated: true, completion: nil)
        }
    }
    
    
    
    @objc func runPromo(_ timer: Timer) {
        NotificationCenter.default.post(name: .PGPromoTimerFireNotification,
                                        object: timer.userInfo)
    }
    
    @objc func stopTrialBy(_ timer: Timer) {
        
        guard let userInfo = timer.userInfo as? [String: Any],
            let stateToSwitch = userInfo["ApplicationState"] as? ApplicaitonState else {
                fatalError("No timer provided")
        }
        
        if stateToSwitch == .trialLong {
            let _ = customKeychainInstance.set(stateToSwitch.rawValue, forKey: KeychainKeys.applicationState.rawValue)
        } else if stateToSwitch == .trialNone {
            let _ = customKeychainInstance.set(stateToSwitch.rawValue, forKey: KeychainKeys.applicationState.rawValue)
        }
        os_log("end timer call")
    }
    
}
