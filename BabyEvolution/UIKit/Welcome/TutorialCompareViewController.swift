//
//  ChildNameViewController.swift
//  BabyEvolution
//
//  Created by Yaroslav on 28.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import YandexMobileMetrica

class TutorialCompareViewController: PGViewController {

    private let headerText = NSLocalizedString("WelcomeStoryboard.ChildSetupNameViewController.header.text",
                                               comment: "App child name setup screen for newcommers, header.")
    private let bodyText = NSLocalizedString("WelcomeStoryboard.ChildSetupNameViewController.textBody.text",
                                             comment: "App welcome screen for returned people, text body.")
    private let nextButtonTitle = NSLocalizedString("WelcomeStoryboard.ChildSetupNameViewController.nextButton.titleLabel.text",
                                                    comment: "App child name setup screen for newcommers, next button title.")
    let videoURL = Bundle.main.url(forResource: "Tutorial/tutorial 3", withExtension: "mov")
    
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var dropShadowView: UIView!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet var header: UILabel!
    
    @IBOutlet var textBody: UITextView!
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet var nextButton: UIButton!
    
    @IBOutlet weak var videoPlayerView: UIView!
    
    @IBOutlet weak var videoShadowView: UIView!
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        guard let coordinator = coordinator as? NewcommerFlowCoordinator else { return }
        YMMYandexMetrica.reportEvent("\(type(of: self)) \(#function)", onFailure: nil)
        coordinator.pushView(causedBy: sender as! UIButton, tappedIn: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textBody.backgroundColor = UIColor.clear
        
        dropShadowView.dropShadow()
        backgroundView.backgroundColor = UIColor.clear
        containerView.layer.cornerRadius = 16
        dropShadowView.layer.cornerRadius = 16
        
        self.navigationController?.isNavigationBarHidden = true
        header.text = headerText
        textBody.text = bodyText
        nextButton.setTitle(nextButtonTitle,
                            for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nextButton.setGradient(with: #colorLiteral(red: 0.4196078431, green: 0.8862745098, blue: 0.4549019608, alpha: 1), endGradientColor: #colorLiteral(red: 0.3921568627, green: 0.7411764706, blue: 0.1843137255, alpha: 1))
    }
    
    override func viewWillLayoutSubviews() {
        containerView.setGradient(with: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), endGradientColor: #colorLiteral(red: 1, green: 0.8941176471, blue: 0.9019607843, alpha: 1))
        view.setGradient(with: #colorLiteral(red: 0.8117647059, green: 0.4862745098, blue: 0.462745098, alpha: 1), endGradientColor: #colorLiteral(red: 0.4941176471, green: 0.2392156863, blue: 0.3215686275, alpha: 1))
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (context) in
            self.containerView.setGradient(with: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), endGradientColor: #colorLiteral(red: 1, green: 0.8941176471, blue: 0.9019607843, alpha: 1))
            self.view.setGradient(with: #colorLiteral(red: 0.8117647059, green: 0.4862745098, blue: 0.462745098, alpha: 1), endGradientColor: #colorLiteral(red: 0.4941176471, green: 0.2392156863, blue: 0.3215686275, alpha: 1))
        }) { (context) in
        }
    }
}
