//
//  WelcomeViewController.swift
//  BabyEvolution
//
//  Created by Yaroslav on 27.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper
import StoreKit
import YandexMobileMetrica

class WelcomeViewController: PGViewController {
    
    private let headerText = NSLocalizedString("WelcomeStoryboard.WelcomeViewController.header.text",
                                              comment: "Welcome App screen header.")
    
    private let bodyText = NSLocalizedString("WelcomeStoryboard.WelcomeViewController.textBody.text",
                                             comment: "Body text for App Welcome screen.")
    
    private let nextButtonTitle = NSLocalizedString("WelcomeStoryboard.WelcomeViewController.nextButton.titleLabel.text",
                                                    comment: "Welcome App screen next button title.")
    
    let customKeychainInstance = KeychainWrapper(serviceName: "ru.neatness.baby-evolution")
    
    
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var dropShadowView: UIView!
    
    @IBOutlet weak var containerView: UIView!

    @IBOutlet var header: UILabel!
    
    @IBOutlet var textBody: UITextView!
    
    @IBOutlet var image: UIImageView!
    
    @IBOutlet var nextButton: UIButton!
    
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        guard let coordinator = coordinator as? NewcommerFlowCoordinator else { return }
        YMMYandexMetrica.reportEvent("\(type(of: self)) \(#function)", onFailure: nil)
        coordinator.pushView(causedBy: sender as! UIButton, tappedIn: self)
    }
    
    @IBAction func legalInfoTapped(_ sender: Any) {
        guard let recognizer = sender as? UITapGestureRecognizer else { fatalError() }
        coordinator?.pushView(causedBy: recognizer, tappedIn: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textBody.backgroundColor = UIColor.clear
        
        
        
        
        dropShadowView.dropShadow()
        backgroundView.backgroundColor = UIColor.clear
        
        containerView.layer.cornerRadius = 16
        dropShadowView.layer.cornerRadius = 16
        header.text = headerText
        textBody.text = bodyText
        nextButton.setTitle(nextButtonTitle, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nextButton.setGradient(with: #colorLiteral(red: 0.4196078431, green: 0.8862745098, blue: 0.4549019608, alpha: 1), endGradientColor: #colorLiteral(red: 0.3921568627, green: 0.7411764706, blue: 0.1843137255, alpha: 1))
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillLayoutSubviews() {
        
        containerView.setGradient(with: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), endGradientColor: #colorLiteral(red: 1, green: 0.8941176471, blue: 0.9019607843, alpha: 1))
        view.setGradient(with: #colorLiteral(red: 0.8117647059, green: 0.4862745098, blue: 0.462745098, alpha: 1), endGradientColor: #colorLiteral(red: 0.4941176471, green: 0.2392156863, blue: 0.3215686275, alpha: 1))
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (context) in
            self.containerView.setGradient(with: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), endGradientColor: #colorLiteral(red: 1, green: 0.8941176471, blue: 0.9019607843, alpha: 1))
            self.view.setGradient(with: #colorLiteral(red: 0.8117647059, green: 0.4862745098, blue: 0.462745098, alpha: 1), endGradientColor: #colorLiteral(red: 0.4941176471, green: 0.2392156863, blue: 0.3215686275, alpha: 1))
        }) { (context) in
        }
    }
}
