//
//  SettingsTableViewController.swift
//  BabyEvolution
//
//  Created by Yaroslav on 27.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import MessageUI
import os.log

class SettingsTableViewController: PGTableViewController  {
    
    
    private let navigationItemTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.navigationItem.title",
                                                        comment: "App Settings screen Navigation Bar Title")
            
    private let purchasesTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.PurchasesTableViewCell.title",
                                                   comment: "App settings screen purchases cell title")
    
    private let interestingFactsTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.FactsTableViewCell.title",
                                                          comment: "App settings screen interesting facts cell title")
    
    private let videoInstructionTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.VideoTutorialTableViewCell.title",
                                                          comment: "App settings screen video tutorial cell title")
    
    private let aboutTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.AboutTableViewCell.title",
                                               comment: "App settings screen about app cell title")
    
    private let agreementTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.AgreementTableViewCell.title",
                                                   comment: "App settings screen agreement cell title")
    
    private let licensesTitle = NSLocalizedString("SettingsStoryboard.SettingsTableViewController.LicensesTableViewCell.title",
                                                  comment: "App settings screen licenses cell title")
    
    
    @IBOutlet weak var isRealDimension: UISwitch!
    
    
    @IBOutlet var purchases: UITableViewCell!
    
    @IBOutlet var interestingFacts: UITableViewCell!
    
    @IBOutlet var videoInstruction: UITableViewCell!
    
    @IBOutlet var about: UITableViewCell!
    
    @IBOutlet var agreement: UITableViewCell!
    
    @IBOutlet var licenses: UITableViewCell!
    
    @IBAction func isRealDimensionSwitchTapped(_ sender: Any) {
        guard let isDimension = sender as? UISwitch else { fatalError("Failed to unwrap isDimension Switch") }
        IS_REAL_DIMENSION = isDimension.isOn
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navigationItemTitle
        purchases.textLabel?.text = purchasesTitle
        interestingFacts.textLabel?.text = interestingFactsTitle
        videoInstruction.textLabel?.text = videoInstructionTitle
        about.textLabel?.text = aboutTitle
        agreement.textLabel?.text = agreementTitle
        licenses.textLabel?.text = licensesTitle
        
        navigationController?.navigationBar.prefersLargeTitles = true

        navigationController?.isNavigationBarHidden = false
    }

    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let controller = MFMailComposeViewController()
            controller.mailComposeDelegate = self
            controller.modalPresentationStyle = .formSheet
            controller.setSubject(NSLocalizedString("ARSceneViewController+TimerLogic.manageRateUsAlert.rateUsBadAction.rateUsBadFlowAlertEmailAction.emailHandler.emailSubject.neutral", comment: ""))
            controller.setToRecipients(["support@neatness.ru"])
            present(controller, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        isRealDimension.isOn = IS_REAL_DIMENSION
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath)!
        if cell.restorationIdentifier == "AppSettings.ContactUs" {
            sendEmail()
        } else if cell.restorationIdentifier == "AppSettings.VideoTutorial" {
            coordinator?.pushView(causedBy: cell as UIView, tappedIn: self)
        } else if cell.restorationIdentifier == "AppSettings.Review" {
            if let url = URL(string: "https://itunes.apple.com/ru/app/id1437782565?action=write-review") {
                UIApplication.shared.open(url, options: [:], completionHandler: { (status) in
                })
            }
        } else {
            coordinator?.pushView(causedBy: tableView.cellForRow(at: indexPath)! as Any, tappedIn: self)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }

}

extension SettingsTableViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}
