//
//  LegalInformationViewController.swift
//  BabyEvolution
//
//  Created by Yaroslav on 28.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit


protocol LegalInformationDataSource {
    var requestedData: String? { get set }
}

class LegalInformationViewController: PGViewController {

//    var navigationItemTitle = NSLocalizedString("SettingsStoryboard.LegalInformationViewController.navigationItem.title", comment: "Legal Information screen title")
    var navigationItemTitle: String?
    var legalText: String?
    @IBOutlet weak var legalTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = navigationItemTitle
        legalTextView.text = legalText
    }

}
