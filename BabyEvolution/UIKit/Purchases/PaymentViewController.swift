//
//  PaymentViewController.swift
//  BabyEvolution
//
//  Created by Yaroslav on 28.08.2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import StoreKit
import os.log
import YandexMobileMetrica
import SwiftKeychainWrapper

class PaymentViewController: PGViewController, Paymentable {
    
    @IBAction func appStateDrop(_ sender: Any) {
        let _ = customKeychainInstance.removeAllKeys()
    }
    
    private let navigationItemTitle = NSLocalizedString("PaymentStoryboard.PaymentViewController.navigationItem.title",
                                                        comment: "Purchases screen title")
    
    private let customKeychainInstance = KeychainWrapper(serviceName: "ru.neatness.baby-evolution")
    
    var products: [String: SKProduct]?
    
    var isOnStart = true
    
    var didPurchased = false
    
    var productsRequestTimeOutTimer = Timer()
    
    @IBOutlet weak var firstPackMainView: UIView!
    @IBOutlet weak var firstPackTitle: UILabel!
    @IBOutlet weak var firstPackSubtitle: UILabel!
    @IBOutlet weak var firstPackPrice: UILabel!
    @IBOutlet weak var firstPackBorderLine: UIView!
    @IBOutlet weak var firstPackBuyLabel: UILabel!
    
    
    @IBOutlet weak var secondPackMainView: UIView!
    @IBOutlet weak var secondPackTitle: UILabel!
    @IBOutlet weak var secondPackSubtitle: UILabel!
    @IBOutlet weak var secondPackPrice: UILabel!
    @IBOutlet weak var secondPackBorderLine: UIView!
    @IBOutlet weak var secondPackBuyLabel: UILabel!
    
    @IBOutlet weak var thirdPackMainView: UIView!
    @IBOutlet weak var thirdPackTitle: UILabel!
    @IBOutlet weak var thirdPackSubtitle: UILabel!
    @IBOutlet weak var thirdPackPrice: UILabel!
    @IBOutlet weak var thirdPackBorderLine: UIView!
    @IBOutlet weak var thirdPackBuyLabel: UILabel!
    
    
    @IBOutlet weak var allPackMainView: UIView!
    @IBOutlet weak var allPacksTitle: UILabel!
    @IBOutlet weak var allPacksSubtitle: UILabel!
    @IBOutlet weak var allPacksPrice: UILabel!
    @IBOutlet weak var allPacksBorderLine: UIView!
    @IBOutlet weak var allPacksBuyLabel: UILabel!
    
    @IBOutlet weak var settingsButton: UIButton!
    
    @IBAction func settingsDidTapped(_ sender: Any) {
        nextButtonTapped(sender)
    }
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    let productsRequestDispatchGroup = DispatchGroup()
    
    
    
    @IBOutlet weak var selectFirstPackSubview: UIView!
    
    @IBOutlet weak var selectSecondPackSubview: UIView!
    
    @IBOutlet weak var selectThirdPackSubview: UIView!
    
    @IBOutlet weak var selectAllPacksSubview: UIView!
    
    @IBOutlet weak var nextButton: UIButton!
    
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        YMMYandexMetrica.reportEvent("\(type(of: self)) \(#function)",
            parameters: ["isOnStart": isOnStart],
            onFailure: nil)
        if isOnStart, !didPurchased {
            guard let coordinator = coordinator as? NewcommerFlowCoordinator else { return }
            UserDefaults.standard.set(PackNumber.first.rawValue, forKey: UserDefaultsKeys.selectedModelsPack.rawValue)
            let alertViewController = UIAlertController(title: NSLocalizedString("PaymentViewController.nextButtonTapped.notDidPurchased.alertViewController.title", comment: "PaymentViewController.nextButtonTapped.notDidPurchased.alertViewController.title, default: Trial period"),
                                                        message: NSLocalizedString("PaymentViewController.nextButtonTapped.notDidPurchased.alertViewController.message", comment: "PaymentViewController.nextButtonTapped.notDidPurchased.alertViewController.message, default: You haven't choose any of the packs to purchase. Therefore you will be provided to the trial period of the app functionality for 5 minutes each day. After the 5 minutes exceed the embryo model will become opaque and you will not be able to change it."),
                                                        preferredStyle: .alert)
            let alertAction = UIAlertAction(title: NSLocalizedString("PaymentViewController.nextButtonTapped.notDidPurchased.alertAction.title", comment: ", default: Continue"), style: .default) { (action) in
                DispatchQueue.main.async {
                    coordinator.pushView(causedBy: sender as! UIButton, tappedIn: self)
                }
            }
            alertViewController.addAction(alertAction)
            present(alertViewController, animated: true, completion: nil)
        } else if isOnStart, didPurchased {
            guard let coordinator = coordinator as? NewcommerFlowCoordinator else { return }
            UserDefaults.standard.set(PackNumber.first.rawValue, forKey: UserDefaultsKeys.selectedModelsPack.rawValue)
            coordinator.pushView(causedBy: sender as! UIButton, tappedIn: self)
        } else {
            guard let coordinator = coordinator as? UniversalFlowCoordinator else { return }
            coordinator.pushView(causedBy: sender as! UIButton, tappedIn: self)
        } 
    }

    fileprivate func fillProductInformation() {
        for product in AppStoreProductIdentifier.allCases {
            switch product.rawValue {
            case AppStoreProductIdentifier.firstEmbrioPack.rawValue:
                firstPackTitle.text = NSLocalizedString("PaymentViewController.produtView.firstPackTitle.didPurchased.true", comment: "PaymentViewController.produtView.firstPackTitle.didPurchased.true, default value: ")
                firstPackPrice.text = ""
                firstPackBuyLabel.text = NSLocalizedString("PaymentViewController.produtView.getProductButton.didPurchased.true", comment: "")
                selectFirstPackSubview.layer.cornerRadius = 14
                selectFirstPackSubview.setGradient(with: #colorLiteral(red: 0.4196078431, green: 0.831372549, blue: 0.8862745098, alpha: 1), endGradientColor: #colorLiteral(red: 0.1843137255, green: 0.4431372549, blue: 0.7411764706, alpha: 1))
            case AppStoreProductIdentifier.secondEmbrioPack.rawValue:
                secondPackTitle.text = NSLocalizedString("PaymentViewController.produtView.secondPackTitle.didPurchased.true", comment: "PaymentViewController.produtView.firstPackTitle.didPurchased.true, default value: ")
                secondPackPrice.text = ""
                secondPackBuyLabel.text = NSLocalizedString("PaymentViewController.produtView.getProductButton.didPurchased.true", comment: "")
                selectSecondPackSubview.layer.cornerRadius = 14
                selectSecondPackSubview.setGradient(with: #colorLiteral(red: 0.4196078431, green: 0.831372549, blue: 0.8862745098, alpha: 1), endGradientColor: #colorLiteral(red: 0.1843137255, green: 0.4431372549, blue: 0.7411764706, alpha: 1))
            case AppStoreProductIdentifier.thirdEmbrioPack.rawValue:
                thirdPackTitle.text = NSLocalizedString("PaymentViewController.produtView.thirdPackTitle.didPurchased.true", comment: "PaymentViewController.produtView.firstPackTitle.didPurchased.true, default value: ")
                thirdPackPrice.text = ""
                thirdPackBuyLabel.text = NSLocalizedString("PaymentViewController.produtView.getProductButton.didPurchased.true", comment: "")
                selectThirdPackSubview.layer.cornerRadius = 14
                selectThirdPackSubview.setGradient(with: #colorLiteral(red: 0.4196078431, green: 0.831372549, blue: 0.8862745098, alpha: 1), endGradientColor: #colorLiteral(red: 0.1843137255, green: 0.4431372549, blue: 0.7411764706, alpha: 1))
            case AppStoreProductIdentifier.allEmbrioPacks.rawValue:
                allPackMainView.isHidden = true
                
                // FIXME: not loading all others need to set titles here to all Labels
            default: fatalError("Unexpected switch case in \(#function)")
            }
        }
    }
    
    fileprivate func requestProductsInformation() {
        os_log("PaymentViewController.requestProductsInformation call")
        guard products == nil else {
            products?.forEach({ (key, product) in
                os_log(OSLogType.info, "PaymentViewController.prepareProductData requested product identifier: %@, for key: %@", product.productIdentifier, key)
            })
            os_log(OSLogType.info, "PaymentViewController.prepareProductData.products != nil")
            fillProductInformation()
            self.productsRequestDispatchGroup.leave()
            return
        }
        EmbryosPack.store.requestProducts { [weak self] success, products in
            guard let self = self else { os_log("EmbryosPack.store.requestProducts.self == nil"); return }
            if success,
                let unwrapedProducts = products {
                self.products = unwrapedProducts.reduce(into: [String: SKProduct](), { (result, item) in
                    os_log(OSLogType.info, "PaymentViewController.prepareProductData requested product identifier: %@", item.productIdentifier)
                    result[item.productIdentifier] = item
                })
                
                DispatchQueue.main.async {
                    self.fillProductInformation()
                }
                os_log(OSLogType.info, "PaymentViewController.store.productRequest succeed, products length: %d", unwrapedProducts.count)
                self.productsRequestDispatchGroup.leave()
            } else {
                os_log(OSLogType.error, "PaymentViewController.store.productRequest failed, request status: %@", success)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mainViewGradient = CAGradientLayer()
        mainViewGradient.frame = view.bounds
        mainViewGradient.colors = [#colorLiteral(red: 0.8117647059, green: 0.4862745098, blue: 0.462745098, alpha: 1).cgColor, #colorLiteral(red: 0.4941176471, green: 0.2392156863, blue: 0.3215686275, alpha: 1).cgColor]
        mainView.layer.insertSublayer(mainViewGradient, at: 0)
        
        let packViews = [
            firstPackMainView,
            secondPackMainView,
            thirdPackMainView,
            allPackMainView
        ]
        
        for packView in packViews {
            packView!.layer.cornerRadius = 16
            let packViewGradientLayer = CAGradientLayer()
            packViewGradientLayer.frame = packView!.bounds
            packViewGradientLayer.colors = [#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor,#colorLiteral(red: 1, green: 0.8941176471, blue: 0.9019607843, alpha: 1).cgColor]
            packViewGradientLayer.cornerRadius = packView!.layer.cornerRadius
            packView!.dropShadow()
            packView!.layer.insertSublayer(packViewGradientLayer, at: 0)
        }
        
        let packBorderLines = [
            firstPackBorderLine,
            secondPackBorderLine,
            thirdPackBorderLine,
            allPacksBorderLine
        ]
        
        for packBorderLine in packBorderLines {
            let borderLineViewGradientLayer = CAGradientLayer()
            borderLineViewGradientLayer.frame = packBorderLine!.bounds
            borderLineViewGradientLayer.colors = [#colorLiteral(red: 0.8549019608, green: 0.5960784314, blue: 0.5764705882, alpha: 1).cgColor, #colorLiteral(red: 0.8549019608, green: 0.5960784314, blue: 0.5764705882, alpha: 0).cgColor]
            borderLineViewGradientLayer.opacity = 0.3
            borderLineViewGradientLayer.startPoint = CGPoint(x: 0.0,
                                                             y: borderLineViewGradientLayer.frame.maxY / 2)
            borderLineViewGradientLayer.endPoint = CGPoint(x: 1.0,
                                                           y: borderLineViewGradientLayer.frame.maxY / 2)
            packBorderLine!.layer.addSublayer(borderLineViewGradientLayer)
        }
        
        
        nextButton.layer.cornerRadius = 14
        
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
        
        navigationController?.isNavigationBarHidden = true
        if isOnStart {
            settingsButton.isHidden = true
        } else {
            settingsButton.isHidden = false
        }
        hideLoadingUI()
        fillProductInformation()
//        DispatchQueue.global().sync {
//            os_log(OSLogType.info, "PaymentViewController.showLoadingUI call")
//            showLoadingUI()
//            productsRequestDispatchGroup.enter()
////            requestProductsInformation()
//
//
//            productsRequestDispatchGroup.notify(queue: .main) {
//                os_log(OSLogType.info, "PaymentViewController.hideLoadingUI call")
//                self.hideLoadingUI()
//            }
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        mainView.setGradient(with: #colorLiteral(red: 0.8117647059, green: 0.4862745098, blue: 0.462745098, alpha: 1), endGradientColor: #colorLiteral(red: 0.4941176471, green: 0.2392156863, blue: 0.3215686275, alpha: 1))
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (context) in
            self.mainView.setGradient(with: #colorLiteral(red: 0.8117647059, green: 0.4862745098, blue: 0.462745098, alpha: 1), endGradientColor: #colorLiteral(red: 0.4941176471, green: 0.2392156863, blue: 0.3215686275, alpha: 1))
        }) { (context) in
        }
    }
    
    
    @IBAction func productToPurchaseViewDidTapped(_ sender: Any) {
        guard let recognizer = sender as? UITapGestureRecognizer else { fatalError() }
        handleTapOnPackView(with: recognizer)
    }
    
    

    private func showLoadingUI() {
        spinner.startAnimating()
        spinner.isHidden = false
        blurView.isHidden = false
        cancelButton.isHidden = true
    }
    
    private func hideLoadingUI() {
        spinner.stopAnimating()
        spinner.isHidden = true
        blurView.isHidden = true
        cancelButton.isHidden = false
    }
    
    fileprivate func handleTapOnPackView(with recognizer: UITapGestureRecognizer) {
        let viewIdentifier = recognizer.view!.restorationIdentifier!
        recognizer.view?.isUserInteractionEnabled = false
        let packIdentifier = AppStoreProductIdentifier(from: PackIdentifier(from: viewIdentifier))
        let packNumber = packIdentifier.returnNumber()

        UserDefaults.standard.set(packNumber.rawValue, forKey: UserDefaultsKeys.selectedModelsPack.rawValue)
        os_log("UserDefaults key \"SelectedModelsPack\": %i",
                   UserDefaults.standard.integer(forKey: UserDefaultsKeys.selectedModelsPack.rawValue))
        
        coordinator?.pushView(causedBy: recognizer.view!, tappedIn: self)
        recognizer.view?.isUserInteractionEnabled = true
    
    }
}
