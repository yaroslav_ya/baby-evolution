//
//  PromoPackViewController.swift
//  BabyEvolution
//
//  Created by Yaroslav on 24/09/2018.
//  Copyright © 2018 Yaroslav. All rights reserved.
//

import UIKit
import StoreKit
import SwiftKeychainWrapper
import YandexMobileMetrica
import os.log


protocol StateChangable {
    func changeAppStateOnPurchase()
}

class PromoPackViewController: PGViewController, Paymentable {
    
    let firstPackIcon = #imageLiteral(resourceName: "Early Embryo Icon")
    let secondPackIcon = #imageLiteral(resourceName: "Late Embryo Icon")
    let thirdPackIcon = #imageLiteral(resourceName: "Middle Embryo Icon")
    
    var isOnStart: Bool?
    var productToPromote: SKProduct?
    
    private let customKeychainInstance = KeychainWrapper(serviceName: "ru.neatness.baby-evolution")
    
    let productsRequestDispatchGroup = DispatchGroup()
    
    let localizedPackCount = NSLocalizedString("", comment: "")
    let localizedPackName = NSLocalizedString("", comment: "")
    let localizedPackBuyButton = NSLocalizedString("PromoPackViewController.BuyButtonView.getProductButtonTitle", comment: "")
    
    let paymentDispatchGroup = DispatchGroup()
    
    var delegate: StateChangable?
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var packViewBackground: UIView!
    @IBOutlet weak var percentSignView: UIView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBOutlet weak var blurView: UIVisualEffectView!

    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productCount: UILabel!
    
    @IBOutlet weak var buyButtonView: UIView!
    @IBOutlet weak var buyByttonLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.layer.cornerRadius = 20
        
        buyButtonView.setGradient(with: #colorLiteral(red: 0.4196078431, green: 0.8862745098, blue: 0.4549019608, alpha: 1), endGradientColor: #colorLiteral(red: 0.3921568627, green: 0.7411764706, blue: 0.1843137255, alpha: 1))
        
        NotificationCenter.default.addObserver(self, selector: #selector(handlePaymentNotification(_:)),
                                               name: .PGIAPHelperPurchaseNotification,
                                               object: nil)
        
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        YMMYandexMetrica.reportEvent("\(type(of: self)) \(#function)",
            parameters: ["isOnStart": isOnStart!],
            onFailure: nil)
        
        dismiss(animated: true, completion: nil)
        guard let isOnStart = isOnStart else { fatalError() }
        showLoadingUI()
        if isOnStart{
            guard let coordinator = coordinator as? SecondtimeCommerCoordinator else { return }
            coordinator.pushView(causedBy: sender as! UIButton, tappedIn: self)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global().sync {
            
            productsRequestDispatchGroup.enter()
            prepareProductData()
            
            productsRequestDispatchGroup.notify(queue: .main) {
                os_log("PromoPackViewController.viewWillAppear.hideLoadingUI call")
                self.hideLoadingUI()
            }
        }
        packViewBackground.setGradient(with: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), endGradientColor: #colorLiteral(red: 1, green: 0.8941176471, blue: 0.9019607843, alpha: 1))
    }
    
    fileprivate func prepareProductData() {
        os_log("PromoPackViewController.prepareProductData.showLoadingUI call")
        showLoadingUI()
        EmbryosPack.store.requestProducts { (success, products) in
            if success,
                let unwrapedProducts = products {
                unwrapedProducts.forEach({ (product) in
                    os_log("PromoPackViewController.prepareProductData requested product identifier: %@", product.productIdentifier)
                })
                
                for product in unwrapedProducts {
                    if !EmbryosPack.store.isProductPurchased(product.productIdentifier) && product.productIdentifier != AppStoreProductIdentifier.allEmbrioPacks.rawValue {
                        self.productToPromote = product
                        self.productsRequestDispatchGroup.leave()
                        break
                    }
                }
                os_log(OSLogType.info, "PromoPackViewController.store.productRequest succeed, products length: %d", unwrapedProducts.count)
            } else {
                os_log(OSLogType.error, "PromoPackViewController.store.productRequest failed, request status: %@", success)
            }
        }
    }
    
    func fillProductInformation() {
        guard let product = productToPromote else { fatalError("Method called before product successful requested.") }
        productTitle.text = product.localizedTitle
        switch product.productIdentifier {
        case AppStoreProductIdentifier.firstEmbrioPack.rawValue: productImage.image = firstPackIcon
        case AppStoreProductIdentifier.secondEmbrioPack.rawValue: productImage.image = secondPackIcon
        case AppStoreProductIdentifier.thirdEmbrioPack.rawValue: productImage.image = thirdPackIcon
        default:
            fatalError("Unexpected PackIdentifier")
        }
        buyByttonLabel.text = localizedPackBuyButton.replacingOccurrences(of: "__AMOUNT__", with: String(format: "%@ %@", locale: Locale.current, product.price, Locale.current.currencySymbol ?? "$"))
        
    }
    
    private func showLoadingUI() {
        spinner.startAnimating()
        percentSignView.isHidden = true
        spinner.isHidden = false
        blurView.isHidden = false
        mainView.isUserInteractionEnabled = false
    }
    
    private func hideLoadingUI() {
        spinner.stopAnimating()
        percentSignView.isHidden = false
        spinner.isHidden = true
        blurView.isHidden = true
        fillProductInformation()
        mainView.isUserInteractionEnabled = true
    }
    
    @objc func handlePaymentNotification(_ notification: Notification) {
        guard customKeychainInstance.set(ApplicaitonState.purchased.rawValue, forKey: KeychainKeys.applicationState.rawValue) else { fatalError("ApplicationState key isn't stored") }
    }
    
    @IBAction func productToPurchaseViewDidTapped(_ sender: Any) {
        guard let sender = sender as? UITapGestureRecognizer else { fatalError() }
        
        YMMYandexMetrica.reportEvent("\(type(of: self)) \(#function)",
            parameters: ["isOnStart": isOnStart!],
            onFailure: nil)
        
        guard let productToPromote = productToPromote else { fatalError() }
        showLoadingUI()
        paymentDispatchGroup.enter()
        EmbryosPack.store.buyProduct(productToPromote, with: { success in
            if success {
                guard let isOnStart = self.isOnStart else { fatalError() }
                if isOnStart{
                    guard let coordinator = self.coordinator as? SecondtimeCommerCoordinator else { return }
                    coordinator.pushView(causedBy: sender.view!, tappedIn: self)
                }
                DispatchQueue.main.async {
                    UserDefaults.standard.set(AppStoreProductIdentifier(rawValue: productToPromote.productIdentifier)!.returnNumber().rawValue, forKey: UserDefaultsKeys.selectedModelsPack.rawValue)
                    self.hideLoadingUI()
                    self.delegate?.changeAppStateOnPurchase()
                    self.dismiss(animated: true, completion: nil)
                    self.paymentDispatchGroup.leave()
                }
            } else if !success {
                DispatchQueue.main.async {
                    self.failedPurchaseHandler(self, with: sender, completionHandler: {
                    })
                    self.hideLoadingUI()
                }
            }
        })
    }
}
